class Link

	attr_accessor :nodeInput
	attr_accessor :nodeOutput
	attr_accessor :weight
	attr_accessor :innovation
	attr_accessor :activated

	def initialize(nodeInput, nodeOutput, weight, innovation)
		@nodeInput = nodeInput
		@nodeOutput = nodeOutput
		@weight = weight #Modify how important this link is in the nodeOutput value (usually : (0.0 <= weight <= 1.0))
		@innovation = innovation
		@activated = true
	end

	def to_hash
		hash = {}
		hash["nodeInput"] = @nodeInput.id.to_i
		hash["nodeOutput"] = @nodeOutput.id.to_i
		if @weight.instance_of? Fixnum
			hash["weight"] = @weight.to_i
		else
			hash["weight"] = @weight.to_f
		end
		if @innovation.instance_of? Fixnum
			hash["innovation"] = @innovation.to_i
		else
			hash["innovation"] = @innovation.to_f
		end
		return hash
	end

	def self.createFromHash(hash, node1=nil, node2=nil) #static method so we can call it as a "constructor"
		if node1==nil
			nodeInput = Node.createFromHash(hash.dig("nodeInput"))
		else
			nodeInput = node1
		end
		if node2==nil
			nodeOutput = Node.createFromHash(hash.dig("nodeOutput"))
		else
			nodeOutput = node2
		end
		weight = hash.dig("weight")
		innovation = hash.dig("innovation")

		link = Link.new(nodeInput, nodeOutput, weight, innovation)
		return link
	end

	def equalTest(other)
		if @weight == other.weight && other.innovation == @innovation && other.nodeInput.equalTest(@nodeInput) && other.nodeOutput.equalTest(@nodeOutput)
			return true
		else
			puts "link.equals returned false"
			return false
		end
	end

	def equals(other)
		return (@nodeInput.id == other.nodeInput.id &&
			@nodeOutput.id == other.nodeOutput.id &&
			@innovation == other.innovation && @weight == other.weight)
	end
end


=begin
n1 = Node.new(1,0.1,1)
n2 = Node.new(2,0.3,2)
link = Link.new(n1,n2,0.2,2)
hash = link.to_hash()
pp hash
=end


require ('NeuralNetwork')
require ('Tarot')
require ('AIPlayer')

class Trainer

	def initialize()
		@finished = false
	end

	def game(network1, network2)
		if network1 != nil && network2 != nil
			game = Tarot.new(AIPlayer.new("Network 1", network1), AIPlayer.new("Network 2", network1))
			game.prepare_game

			while !game.is_finished
				game.go_on
			end

			# print game.log # Uncomment to see party logs

			if game.winner == network1 # return the looser to kill him (HARDCOREEEE !!!)
				return network2
			else
				return network1
			end
		end
	end

end

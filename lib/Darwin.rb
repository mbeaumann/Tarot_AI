$LOAD_PATH << File.dirname(__FILE__)
File.join(File.dirname(__FILE__), '../')

require 'java'
require 'fileutils'

java_import 'java.util.concurrent.Callable'
java_import 'java.util.concurrent.ThreadPoolExecutor'
java_import 'java.util.concurrent.TimeUnit'
java_import 'java.util.concurrent.LinkedBlockingQueue'
java_import 'java.util.concurrent.FutureTask'

require('NeuralNetwork.rb')
require('Specie.rb')
require('Node.rb')
require('Link.rb')

class Darwin

	attr_reader :species
	attr_reader :innovation

	def initialize()
		@species = []
		@innovation = 1
		@generation = 1
		@max_population = 128 # arbitrary

		first = Specie.new()
		@species.push(first)
		firstNetwork = create_life()
		first.neuralNetworkPool.push(firstNetwork)
	end

	# Initialize the first neuronal network
	def create_life()
		network = NeuralNetwork.new

		id = 1
		outputNodes = []
		while id <= 78 # Creating output nodes (1 for each card)
			output = Node.new(id, 0.0, 2)
			network.add_node(output)
			outputNodes << output
			id += 1
		end
		#########################################################################
		# BEFORE
		# Creating input nodes : 4 * 78 nodes
		# - 78 first nodes represent the cards present in the player's hand
		# - next 78 nodes represent the cards visible on our card stacks
		# - next 78 nodes represent the cards visible on the opponent card stacks
		# - last 78 nodes represent the cards already played during the party
		#########################################################################
		# AFTER
		# - 78 first nodes represent the cards present in the player's hand and
		# 	 	the cards visible on our card stacks (less nodes)
		# - next 78 nodes represent the card played by the opponent
		#########################################################################


		#First input set : cards in hand. Initialized with a link to the corresponding output.
		(1..78).each do
			input = Node.new(id, 0.0, 1)
			network.add_node(input)
			network.add_link(Link.new(input, outputNodes[(id % 78) - 1], 1.0, @innovation))
			id += 1
		end

		#Second input set : last card played in the current trick. Initialized with a link to each output of the same suit.

		(1..14).each do
			input = Node.new(id, 0.0, 1)
			network.add_node(input)
			for y in 0..13
				network.add_link(Link.new(input, outputNodes[y], 1.0, @innovation))
			end
			id += 1
		end

		(1..14).each do
			input = Node.new(id, 0.0, 1)
			network.add_node(input)
			for y in 14..27
				network.add_link(Link.new(input, outputNodes[y], 1.0, @innovation))
			end
			id += 1
		end

		(1..14).each do
			input = Node.new(id, 0.0, 1)
			network.add_node(input)
			for y in 28..41
				network.add_link(Link.new(input, outputNodes[y], 1.0, @innovation))
			end
			id += 1
		end

		(1..14).each do
			input = Node.new(id, 0.0, 1)
			network.add_node(input)
			for y in 42..55
				network.add_link(Link.new(input, outputNodes[y], 1.0, @innovation))
			end
			id += 1
		end

		(1..22).each do
			input = Node.new(id, 0.0, 1)
			network.add_node(input)
			for y in 56..77
				network.add_link(Link.new(input, outputNodes[y], 1.0, @innovation))
			end
			id += 1
		end

		@innovation += 1 # Inc. the innovation number
		return network
	end

	# Dump all the species
	def dump_all(dump_dir="dump")
		id = 1
		@species.each do |specie|
			specie.dump(dump_dir, id, @generation)
			id += 1
		end
		file = File.open(dump_dir + "/darwin", "w+") # Dump innovation number for DARWIN
		file.write(@innovation)
		file.close
	end

	# Loads all specie from the last known innovation
	def load_all(dump_dir="dump")
		file = File.new(dump_dir + "/darwin", "r") # Load last innovation number
		@innovation = file.gets.to_i
		file.close

		@species = [] # Reset
		max = 0
		dir_to_load = ""
		Dir[dump_dir + "/*"].each do |dir|
			next if dir == "darwin" || dir == "." || dir == ".."

			gen = dir[/[0-9]+$/].to_i
			if max < gen
				max = gen
				dir_to_load = dir
			end
		end
		puts "(DARWIN) Generation #{max} loaded"
		@generation = max + 1 # Load last darwin generation

		if !File.directory?(dir_to_load)
			puts "ERROR wihle loading species (#{dir_to_load})... "
			return
		end

		Dir.open dir_to_load do |specie_dir|
			id = 1
			specie_dir.each do |s|
				next if s == "." || s == ".."
				specie = Specie.new
				specie.load(specie_dir.path + "/" + s)
				@species.push(specie)
				id += 1
			end
		end

	end

	class DarwinSpecieSelection
		include Callable

		def initialize(darwin, specie_idx)
			@darwin = darwin
			@specie_idx = specie_idx
		end

		def call()
			@darwin.species[@specie_idx].select
		end
	end

	# Mutate all the species
	def mutate_all()
		fitnesses = []
		sum = 0.0
		@species.each_with_index do |specie, i|
			explicit_fitness_sharing = specie.get_explicit_fitness_sharing
			fitnesses << [explicit_fitness_sharing, i]
			sum += explicit_fitness_sharing
		end

		offsprings = []
		if @species.size != 1
			@species.each_with_index do |specie, i|
				offsprings << ((fitnesses[i][0] / sum).to_f * @max_population).to_i
			end
			offsprings.sort { |x, y| y <=> x }
			fitnesses.sort { |x, y| x[0] <=> y[0] }
		else
			offsprings << 128
		end

		# Attributing the less offsprings to the strongest specie
		threads = []
		new_networks = []
		fitnesses.each_with_index do |arr, i|
			if @species[arr[1]].neuralNetworkPool.size > 1
				threads << Thread.new {
					Thread.current["res"] = @species[arr[1]].make_mutations(@innovation, offsprings[i])
				}
				@innovation += offsprings[i]
			end
		end
		threads.each { |t| t.join; new_networks << t["res"] }

		distance_threshold = 0.2
		new_species = []
		new_networks.flatten!
		new_networks.each do |new_network|
			best_specie = 0
			best_distance = 1.0

			new_species.each_with_index do |specie, i|
				repr = specie.getRepresentative
				distance = new_network.compatibility_distance(repr)
				if distance < best_distance
					best_distance = distance
					best_specie = i
				end
			end

			if best_distance <= distance_threshold
				new_species[best_specie].addNeuralNetwork(new_network)
			else
				s = Specie.new
				s.addNeuralNetwork(new_network)
				new_species << s
			end
		end

		@species.clear
		new_species.each do |specie|
			if specie.neuralNetworkPool.size > 1
				@species << specie
			end
		end
	end

	def select_all()
		num_threads = @species.length
		threads = ThreadPoolExecutor.new(num_threads, num_threads, 60, TimeUnit::SECONDS, LinkedBlockingQueue.new)
		tasks = []

		(0..num_threads - 1).each do |i|
			task = FutureTask.new(DarwinSpecieSelection.new(self, i))
			threads.execute(task)
			tasks << task
		end

		tasks.each do |t|
			t.get
		end

		threads.shutdown
	end

	# Initialize the neural network population by adding "@max_population" networks into the first specie
	def init_population()
		(@max_population - 1).times { @species.first.addNeuralNetwork(create_life()) }
	end

	# Function used to evolve all the species contained by Darwin. It makes the networks in these species fight and keeps the winner to
	# create new networks.
	# Arbitrarily, when 10 network fights have passed, speciation (and dump) is made to create new species from the subsisting networks.
	def evolve()
		running = true
		hunger_games_counter = 1

		if File.exists?("dump/darwin")
			puts "(DARWIN) Found some species to load.\n(DARWIN) Loading..."
			load_all
		else
			init_population
		end

		while running
			t0 = Time.now
			population = 0
			@species.each { |specie| population += specie.neuralNetworkPool.size }
			puts "(DARWIN) Hunger Game #{hunger_games_counter}: #{@species.size} species - #{population} neural networks\n\t> Population :"

			@species.each_with_index do |s, i|
				puts "\t\t- Specie #{i} - #{s.neuralNetworkPool.size} networks;"
			end

			select_all
			t1 = Time.now
			print "\t> Selection... #{t1 - t0} sec\n\t> Mutations... "
			mutate_all
			puts "#{Time.now - t1} sec"

			@generation += 1
			dump_all if hunger_games_counter % 10 == 0

			if hunger_games_counter % 100 == 0
				puts "(DARWIN) Hunger game n° #{hunger_games_counter}\n(DARWIN) \tSpecies : #{@species.length}\n(DARWIN) Do you want to continue ? (y/n)"
				print "\t> "
				entry = gets
				if (entry == "n\n" || entry == "N\n")
					running = false
				end
			end

			hunger_games_counter += 1
		end

		puts "(DARWIN) Evolution stopped.\n(DARWIN) #{hunger_games_counter - 1} Games made."
	end
end

# Pretty simple no ?
#darwin = Darwin.new
#darwin.evolve


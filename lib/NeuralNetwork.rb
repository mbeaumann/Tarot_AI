require_relative "Node.rb"
require_relative "Link.rb"
require 'json'
require 'pp' #for test purpose remove after M beaumann

class NeuralNetwork

	attr_accessor :nodes
	attr_accessor :links

	def initialize()
		@nodes = Hash.new # Here keys are node IDs and values are the corresponding nodes
		@links = Hash.new # Here the keys are node from which the link starts and the value is an array of the links starting from this node
	end

	def add_node(node)
		@nodes.store(node.id, node)
	end

	def add_link(link)
		links = @links.clone
		if links[link.nodeInput.id] == nil
			links[link.nodeInput.id] = []
		end
		links[link.nodeInput.id].each do |existing_link|
			return if existing_link.nodeOutput.id == link.nodeOutput.id
		end
		links[link.nodeInput.id] << link
		@links = links.clone
	end

	def remove_link(link)
		@links[link.nodeInput.id].delete(link)
		@links.delete(link.nodeInput.id) if @links[link.nodeInput.id].empty?
	end

	def remove_node(node)
		return nil if node.type != 0

		@links.delete(node.id) # deleting links starting from node
		@links.values.each do |link_list|
			link_list.each_with_index do |link, i|
				if link.nodeInput.id == node.id
					link_list.delete_at(i)
				elsif link.nodeOutput.id == node.id
					link_list.delete_at(i)
				end
			end
		end
		return @nodes.delete(node.id)
	end

	def reset_node_values()
		@nodes.values.each do |node|
			node.value = 0.0
		end
	end

	def get_random_node()
		rng = Random.new()
		return @nodes[@nodes.keys[rng.rand(@nodes.keys.length)]]
	end

	def get_random_link()
		return nil if @links.keys.empty? #check if there is some links in the network
		rng = Random.new()
		begin
			node = get_random_node
		end while @links[node.id] == nil
		random_link = rng.rand(@links[node.id].length)
		return @links[node.id][random_link]
	end

	def get_links_array()
		links_arr = []
		@links.each_value do |link_list|
			link_list.each { |link| links_arr << link }
		end
		return links_arr
	end

	#Return a node object from an id
	def getNodeById(id)
		return @nodes[id]
	end

	#Walks through all network links and find the neighbours of a given node
	def getNeighboursOf(id)
		neighbours = []

		e_links = @links[id] # check if a node has links to reach other nodes
		if e_links != nil
			@links[id].each do |link|
				neighbours.push(link.nodeOutput) if link.activated
			end
		end
		return neighbours
	end

	#Update the value of a node : subsequently calls itslef on all nodes that are linked with link.in = this and link.out = node
	#Should typically only be used on Inputs node
	def updateNode(id, value)
		visited = [id]
		@nodes[id].value = value

		getNeighboursOf(id).each do |neighbour|
			computeNode(neighbour, visited)
		end
	end

	def computeNode(node, visited)
		# Verify that the actual node hasn't been updated yet
		return if visited.include?(node.id)
		visited << node.id

		#compute value according to previous nodes and weight
		newValue = 0.0
		weightSum = 0.0
		@links.keys.each do |key|
			@links[key].each do |link|
				if link.nodeOutput == node
					newValue += link.nodeInput.value * link.weight
					weightSum += link.weight
				end
			end
		end
		newValue = newValue / weightSum
		node.value = newValue

		# puts("Assigned value " + newValue.to_s)
		getNeighboursOf(node.id).each do |neighbour|
			computeNode(neighbour, visited)
		end
	end

	def sanityChecker()
		#TODO check for circular links (node1 -> node2 && node2 -> node1)
		#Check if this NeuralNetwork have invalid links :
		temp_links = []
		@links.keys.each do |key|
			@links[key].each do |link|
				if link.nodeInput.type == 0 && link.nodeOutput.type == 1
					#Hidden -> Input
					puts "[sanityChecker] Hidden -> Input link"
				elsif link.nodeInput.type == 2
					#Output -> *
					puts "[sanityChecker] Output -> * link"
				else
					#healthy link = keep it
					temp_links.push(link)
				end
			end
		end

		@links = Hash.new # Reset links
		temp_links.each do |healthy_link|
			add_link(healthy_link)
		end
	end

	#Check if a new link creates a cycle in the network
	def isCyclic(newLink)
		visited = Array.new(@nodes.keys.max, false)
		# visited[newLink.nodeInput.id] = true
		visited[newLink.nodeOutput.id] = true

		getNeighboursOf(newLink.nodeOutput.id).each do |neighbour|
			if !visited[neighbour.id]
				isCyclic = isCyclicUtil(neighbour, newLink.nodeInput, visited)
				return true if isCyclic
			end
		end
		return false
	end

	def isCyclicUtil(current, startingNode, visited)
		return true if current.id == startingNode.id
		return false if visited[current.id]
		visited[current.id] = true

		getNeighboursOf(current.id).each do |neighbour|
			if !visited[neighbour.id]
				isCyclic = isCyclicUtil(neighbour, startingNode, visited)
				return true if isCyclic
			end
		end
		return false
	end

	def dump(filename)
		hash = {} #need to convert to a hash to dump it in JSON
		nodeHash = {}
		linkHash = {}

		@nodes.each do |key, value|
			nodeHash[key] = value.to_hash
		end

		get_links_array().each_with_index do |link, i|
			linkHash[i] = link.to_hash
		end

		hash["nodes"] = nodeHash
		hash["links"] = linkHash

		json = JSON.generate(hash)

		begin
			file = File.open(filename, "w+")
			file.write(json)
		rescue IOError => e
			puts "Error while writing neural network to file"
			puts e.to_s
		ensure
			file.close unless file.nil?
		end
	end

	def load(filename)
		jsonStr = ""
		begin
			file = File.new(filename, "r")
			while (line = file.gets)
				jsonStr = jsonStr + line
			end
			file.close
		rescue => err
			puts "Exception: #{err}"
			err
		end
		readHash = {}
		readHash = JSON.parse(jsonStr)

		nodeHash = readHash.dig("nodes")
		linkHash = readHash.dig("links")

		nodeHash.each do |elem|
			n = Node.createFromHash(elem[1])
			@nodes[n.id] = n
		end

		linkHash.each do |elem|
			lHash = elem[1]

			n1 = lHash.dig("nodeInput")
			n2 = lHash.dig("nodeOutput")

			newLink = Link.createFromHash(lHash, @nodes[n1], @nodes[n2])
			add_link(newLink)
			# @links.push(newLink)
		end
	end


	#Take a list of links, returns a integer tab where every "hole" if filled with -1
	#!!! IGNORES 1 !!!
	def util_get_innovation_list(links)
		res = Array.new
		res.push(1)
		for i in 0..links.size - 1
			if(links[i].innovation > 1) #Ignore if innovation number is 1
				if i == 0
					if(links[i] == 2)
						res.push(links[i].innovation)
					else
						y = links[i].innovation - 1
						while(y > 0)
							res.push(-1)
							y -= 1
						end
					end
				elsif(links[i].innovation - links[i - 1].innovation != 1)
					y = links[i].innovation - links[i - 1].innovation - 1
					while(y > 0)
						res.push(-1)
						y -= 1
					end
					res.push(links[i].innovation)
				else
					res.push(links[i].innovation)
				end

			end
		end
		return res
	end

	#Take a list of links, return it with the "holes" between innovation filled with a -1, used by get_weight_average_difference
	#!!! IGNORES 1 !!!
	def util_get_aligned_links(links)
		res = Array.new
		intInnovationList = util_get_innovation_list(get_links_array)

		y = 0
		for i in 0..(intInnovationList.size - 1)
			if intInnovationList[i] == 1 || intInnovationList == -1
				res.push(intInnovationList[i])
			else
				res.push(links[y])
				y += 1
			end

		end

		return res
	end

	# Return the number of disjointed genes between two neural networks
	# !!! IGNORES INNOVATION NUMBER 1, CONSIDERED AS BASIS OF ALL NN !!!
	def get_disjoint_genes(n1_links, n2_links)
		disjointGenes = 0

		localInnovationList = util_get_innovation_list(n1_links)
		n2InnovationList = util_get_innovation_list(n2_links)

		# pp localInnovationList
		# pp n2InnovationList

		thisGeneNum = localInnovationList.size
		n2GeneNum = n2InnovationList.size

		minGenes = [thisGeneNum, n2GeneNum].min

		for i in 0..(minGenes - 1)
			if localInnovationList[i] != n2InnovationList[i]
				disjointGenes += 1
			end
		end

		return disjointGenes
	end

	def get_excess_genes(n1_links, n2_links)
		excessGenes = 0

		thisGeneNum = n1_links.size
		n2GeneNum = n2_links.size

		minGenes = [thisGeneNum, n2GeneNum].min
		maxGenes = [thisGeneNum, n2GeneNum].max

		#Get the amount of excess genes
		excessGenes = maxGenes - minGenes - 1

		return excessGenes
	end

	def get_weight_average_difference(n1_links, n2_links)
		res = 0
		matchingGenes = 0
		weightDiffSum = 0
		thisGeneList = util_get_aligned_links(n1_links)
		n2GeneList = util_get_aligned_links(n2_links)

		minGenes = [thisGeneList.size, n2GeneList.size].min
		for i in 0..(minGenes - 1)
			if thisGeneList[i].class == Link && n2GeneList[i].class == Link
				if(thisGeneList[i].innovation == n2GeneList[i].innovation)
					weightDiff = thisGeneList[i].weight - n2GeneList[i].weight
					weightDiff = weightDiff.abs
					weightDiffSum += weightDiff
					matchingGenes += 1
				end
			end
		end

		if matchingGenes > 1
			res = weightDiffSum / matchingGenes
		end
		return res
	end

	# Compute the compatibility distance between two neural networks
	#
	# c1, c2, c3 are coefficient value adjusting the importance of disjointGenes, excessGenes and weightDiff respectively.
	# Higher coeff means the algorithm is more sensitive to a disjointedGenes/excessGene/weightDiff.
	#
	# See http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf part 3.3 (p 109)
	def compatibility_distance(network2, c1 = 1.0, c2 = 1.0, c3 = 1.0)
		res = 0

		# @links = @links.sort_by(&:innovation)
		# network2.links = network2.links.sort_by(&:innovation)
		sorted_links = get_links_array().sort_by(&:innovation)
		other_sorted_links = network2.get_links_array().sort_by(&:innovation)

		disjointGenes = get_disjoint_genes(sorted_links, other_sorted_links).to_f
		excessGenes = get_excess_genes(sorted_links, other_sorted_links).to_f
		weight_diff = get_weight_average_difference(sorted_links, other_sorted_links)
		largerSize = [sorted_links.size, other_sorted_links.size].max.to_f

		# puts "disjointGenes = " + disjointGenes.to_s
		# puts "excessGenes = " + excessGenes.to_s
		# puts "W = " + weight_diff.to_s
		# puts "N = " + largerSize.to_s

		if largerSize > 0
			res = (c1 * (disjointGenes / largerSize)) +(c2 * (excessGenes / largerSize)) + (c3 * weight_diff)
		end

		return res
	end
end



#network = NeuralNetwork.new()
=begin
node1 = Node.new(1, 0.0, 1)
node12 = Node.new(12, 0.0, 1)
node2 = Node.new(2, 0.0, 2)
node3 = Node.new(3, 0.0, 0)
network.addNode(node1)
network.addNode(node2)
network.addNode(node3)
network.addNode(node12)
network.addLink(Link.new(node1, node3, 1, 1))
network.addLink(Link.new(node12, node3, 0.5, 1))
network.addLink(Link.new(node3, node2, 1, 1))
network.updateNode(1, 1.0)
network.updateNode(12, 1.0)
#network.dump("neural.json")
#network.load("neural.json")
#pp network #pp : Pretty Print
=end


$LOAD_PATH << File.dirname(__FILE__)

require('CardStack.rb')
require('PhysicalPlayer')
require('AIPlayer')

class Tarot

	attr_reader :winner
	attr_reader :log

	BID_NO_BID = 0
	BID_SMALL = 1
	BID_GUARD = 2
	BID_GUARD_WITHOUT_KITTY = 3
	BID_GUARD_AGAINST_KITTY = 4

	def initialize(player1, player2)
		@player1 = player1
		@player2 = player2
		@is_finished = false
        @log = ""
	end

	def prepare_game()
		card_stack = CardStack.new
		card_stack.mix_cards
		kitty = Array.new(6) { card_stack.distribute() }

		@player1.give_hand(Array.new(20) { card_stack.distribute() })
		@player2.give_hand(Array.new(20) { card_stack.distribute() })
		@player1.give_stacks(Array.new(4) { Array.new(4) { card_stack.distribute() }})
		@player2.give_stacks(Array.new(4) { Array.new(4) { card_stack.distribute() }})

		bids(kitty)
	end

	def bids(kitty)
		bid1 = @player1.bet
		bid2 = @player2.bet

		if bid1 == BID_GUARD_AGAINST_KITTY
			@better = @player1
			@party_bid = bid1
			@player2.set_kitty(kitty, bid1)
		elsif bid2 == BID_GUARD_AGAINST_KITTY
			@player1.set_kitty(kitty, bid1)
			@party_bid = bid2
			@player2.set_kitty(kitty, bid1)
		else
			if bid1 > bid2
				overbid = @player2.overbid(bid1)
				if overbid > bid1
					@player2.set_kitty(kitty, overbid)
					@better = @player2
					@party_bid = overbid
				else
					@player1.set_kitty(kitty, bid1)
					@better = @player1
					@party_bid = bid1
				end
			elsif bid1 < bid2
				overbid = @player1.overbid(bid2)
				if overbid > bid2
					@player1.set_kitty(kitty, overbid)
					@better = @player1
					@party_bid = overbid
				else
					@player2.set_kitty(kitty, bid2)
					@better = @player2
					@party_bid = bid2
				end
			else
				if bid1 == BID_NO_BID && bid2 == BID_NO_BID
					@log += "(GAME) None is making an auction... \n"
					@is_finished = true
					@winner = @player1 # Yes, it's unfair
					return
				end
			end
		end
		if @better == @player1
			@first_player = @player1
			@last_player = @player2
		else
			@first_player = @player2
			@last_player = @player1
		end
		@log += "(GAME) Party bid : #{@party_bid}. \n"
	end

	def go_on()
		if @player1.hand.length == 0 && @player1.get_visible_stack_cards().size == 0 && @player2.hand.length == 0 && @player2.get_visible_stack_cards().size == 0
			end_game(true)
			return
		end

		card1 = @first_player.play
		if card1 == nil
			@log += "(GAME) #{@first_player.name} played a card he doesn't have (got nil). \n"
			end_game(false, @last_player)
			return
		end
		card2 = @last_player.play(card1)
		@log += "(GAME) #{@first_player.name} played #{card1} || #{@last_player.name} played #{card2}. \n"
		if !(check_validity(card1, card2))
			end_game(false, @first_player)
			return
		end

		score = get_trick_winner(card1, card2)
		@first_player = score[0]
		@last_player  = score[1]
		@first_player.add_trick([card1, card2])
	end

	def end_game(game_ended_normally, winner=nil)
		if game_ended_normally
			player1_score = 0
			player2_score = 0
			@first_player.tricks.each do |card|
				player1_score += card.get_score_value
			end
			@last_player.tricks.each do |card|
				player2_score += card.get_score_value
			end

			@log += "(GAME) Party bid: #{@party_bid}\n#{@first_player.name}'s score: #{player1_score}\n#{@last_player.name}'s score: #{player2_score}\n"

			if player1_score > player2_score
				@winner = @first_player
			else
				@winner = @last_player
			end
			@log += "(GAME) Game exited normally, winner ==> #{@winner.name}. \n"
		else
			@winner = winner
			@log += "(GAME) Game doesn't exited normally... Winner ==> #{@winner.name}. \n"
		end
		@is_finished = true
	end

	def get_trick_winner(card1, card2)
		if card1.suit == card2.suit
			if card1.number > card2.number
				return [@first_player, @last_player]
			else
				return [@last_player, @first_player]
			end
		else # Different suits, trumps win else, the first player win
			if card1.suit == 5
				return [@first_player, @last_player]
			elsif card2.suit == 5
				return [@last_player, @first_player]
			end
		end
		return [@first_player, @last_player]
	end

	def check_validity(card1, card2)
		# @log += "(GAME) Checking validity of #{card1} and #{card2}. \n"
		if card2 == nil
			@log += "(GAME) #{@last_player.name} played a card he don't have. \n"
			return false
		end

		if card1.suit != card2.suit
			if @last_player.have_suit(card1.suit) # Play a different card when having another card with the same suit
				@log += "(GAME) #{@last_player.name} played a different card when having another card with the same suit. \n"
				return false
			end
		else
			if card2.suit == 5 && card2.number < card1.number && @last_player.has_bigger_trump(card1)
				@log += "(GAME) #{@last_player.name} played a lower trump than this of its opponent when having a higher trump. \n"
		 		return false
			end
		end
		return true
	end

	def is_finished()
		return @is_finished
	end
end

# MAIN
# player1 = PhysicalPlayer.new("Player 1")
# player2 = PhysicalPlayer.new("Player 2")
# game = Tarot.new(player1, player2)
# game.prepare_game
#
# while !game.is_finished
# 	game.go_on
# end
#
# if game.winner == player1 # return the looser to kill him (HARDCOREEEE !!!)
# 	return player2
# else
# 	return player1
# end

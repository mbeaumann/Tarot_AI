class Card

	attr_reader :suit
	attr_reader :number

	def initialize(suit, number)
		if suit == 5 and (number < 100 or number > 121)
			puts "Invalid number (#{number}) for given suit (Trump Card)"
		elsif suit != 5 and (number < 1 or number > 14)
			puts "Invalid number (#{number}) for given suit (#{suit})"
		else
			@suit = suit
			@number = number
		end
	end

	def to_s
		case @suit
		when 1..4
			return Number.stringify(@number) + " of " + Suit.stringify(@suit)
		when 5
			return Number.stringify(@number)
		end
	end

	def get_score_value
		case @number
		when 1..10
			0.5
		when 11
			2
		when 12
			3
		when 13
			4
		when 14
			5
		when 101..121
			0.5
		else
			0
		end
	end
end

# Enumeration classes, used for reference

class Suit
	Club=1
	Spade=2
	Diamond=3
	Heart=4
	Trump=5

	def self.stringify(num)
		case num
		when 1
			"Club"
		when 2
			"Spade"
		when 3
			"Diamond"
		when 4
			"Heart"
		when 5
			"Trump"
		else
			"Unknown suit"
		end
	end
end

class Number
	Ace=1
	Two=2
	Three=3
	Four=4
	Five=5
	Six=6
	Seven=7
	Eight=8
	Nine=9
	Ten=10
	Jack=11
	Knight=12
	Queen=13
	King=14

	The_Fool=100
	The_Magician=101
	The_High_Priestess=102
	The_Empress=103
	The_Emperor=104
	The_Hierophant=105
	The_Lovers=106
	The_Chariot=107
	Strength=108
	The_Hermit=109
	Wheel_Of_Fortune=110
	Justice=111
	The_Hanged_Man=112
	Death=113
	Temperance=114
	The_Devil=115
	The_Tower=116
	The_Star=117
	The_Moon=118
	The_Sun=119
	Judgement=120
	The_World=121

	def self.stringify(num)
		case num
		when 1
			"Ace"
		when 2..10
			num.to_s
		when 11
			"Jack"
		when 12
			"Knight"
		when 13
			"Queen"
		when 14
			"King"
		when 100
			"0 - The Fool"
		when 101
			"1 - The Magician"
		when 102
			"2 - The High Priestess"
		when 103
			"3 - The Empress"
		when 104
			"4 - The Emperor"
		when 105
			"5 - The Hierophant"
		when 106
			"6 - The Lovers"
		when 107
			"7 - The Chariot"
		when 108
			"8 - Strength"
		when 109
			"9 - The Hermit"
		when 110
			"10 - Wheel Of Fortune"
		when 111
			"11 - Justice"
		when 112
			"12 - The Hanged Man"
		when 113
			"13 - Death"
		when 114
			"14 - Temperance"
		when 115
			"15 - The Devil"
		when 116
			"16 - The Tower"
		when 117
			"17 - The Star"
		when 118
			"18 - The Moon"
		when 119
			"19 - The Sun"
		when 120
			"20 - Judgement"
		when 121
			"21 - The World"
		else
			"Unknown value"
		end
	end
end

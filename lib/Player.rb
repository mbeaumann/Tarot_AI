
$LOAD_PATH << File.dirname(__FILE__)

require('Card.rb')

class Player
	attr_reader :hand
	attr_reader :stacks
	attr_reader :tricks
	attr_reader :name

	BID_NO_BID = 0
	BID_SMALL = 1
	BID_GUARD = 2
	BID_GUARD_WITHOUT_KITTY = 3
	BID_GUARD_AGAINST_KITTY = 4

	def initialize(name)
		@hand = []
		@stacks = []
		@tricks = []
		@name = name
	end

	def give_hand(hand)
		order_Hand(hand)
	end

	def give_stacks(stacks)
		@stacks = stacks
	end

	def print_hand(i)
		puts @name + "'s hand cards :"
		for card in @hand
			print "\t" + i.to_s + ") " + card.to_s + "\n"
			i+=1
		end
	end

	def print_stacks(i)
		puts "On top of the stacks :"
		for stack in @stacks
			if !stack.empty?
				print "\t" + i.to_s + ") " + stack.first.to_s + "\n"
				i+=1
			end
		end
	end

	def print_cards()
		print_hand(1)
		print_stacks(@hand.length + 1)
	end

	def add_trick(cards)
		cards.each do |card|
			@tricks.push(card)
		end
	end

	# Reset an invalid card played by the player in is hand
	def invalid_move(played_card)
		@hand.push(played_card)
		order_Hand(@hand)
	end

	def has_bigger_trump(card)
		@hand.each do |c|
			if c.suit == card.suit && c.number > card.number
				return true
			end
		end
		get_visible_stack_cards().each do |c|
			if c.suit == card.suit && card.number > card.number
				return true
			end
		end
		return false
	end

	def to_s
		res = "Hand : " + @hand.join(", ")
		res = res + "\n"
		for i in 0..3
			res = res + "Stack " + (i + 1).to_s + " : " + @stacks[i].join(", ")
			res = res + "\n"
		end
		res
	end

	def order_Hand(hand)
		spade = []
		club = []
		heart = []
		diamond = []
		trump = []

		#placing each card from @hand in an array dpending on its suit
		hand.each do |card|
			case card.suit
			when 1
				club.push(card)
			when 2
				spade.push(card)
			when 3
				diamond.push(card)
			when 4
				heart.push(card)
			when 5
				trump.push(card)
			end
		end

		club = club.sort_by(&:number)
		spade = spade.sort_by(&:number)
		heart = heart.sort_by(&:number)
		diamond = diamond.sort_by(&:number)
		trump = trump.sort_by(&:number)

		result = []

		result.push(spade).push(heart).push(club).push(diamond).push(trump).flatten!

		@hand = result
	end

	def has_card(card)
		return false if card == nil
		@hand.each do |c|
			return true if c.suit == card.suit && c.number == card.number
		end
		get_visible_stack_cards().each do |c|
			return true if c.suit == card.suit && c.number == card.number
		end
		return false
	end

	def remove_card(card)
		if card == nil
			raise "Method \"remove_card\" from Played.rb received a nil card..."
			return nil
		end

		@hand.each_with_index do |c, i|
			if c.suit == card.suit && c.number == card.number
				return @hand.delete_at(i)
			end
		end
		@stacks.each_with_index do |stack, i|
			next if stack.empty?
			c = stack[0]
			if c.suit == card.suit && c.number == card.number
				return @stacks[i].delete_at(0)
			end
		end
		raise "Attempting to remove a card that the player doesn't have...."
	end

	#Check if suit is in the player hand or at the top of a stack
	def have_suit(suit)
		hand.each do |card|
			if card.suit == suit
				return true
			end
		end
		for stack in @stacks
			if !stack.empty?
				if stack.first.suit == suit
					return true
				end
			end
		end
		return false
	end

	def get_visible_stack_cards()
		visibles = []
		@stacks.each do |stack|
			if !stack.empty?
				visibles << stack[0]
			end
		end
		return visibles
	end

	# Evaluate the value of the hand
	# Does NOT consider the hidden cards in the stacks : there is no accurate way to account for them
	#
	#
	# Recommended auctions (NEED TWEAKING FOR 2 PLAYERS VERSION) :
	# 40pts : Small Bid
	# 56pts : Guard
	# 71pts : Guard without kitty
	# 81pts : Guard against kitty
	#
	# Made following the guide at http://www.le-tarot.fr/quel-contrat-choisir/
	def hand_value(array = @hand)
		score = 0

		guard = 56
		above_guard = 71

		#Sort all cards by suit : we use the cards in @hands and the top of the stacks

		spade = []
		club = []
		heart = []
		diamond = []
		trump = []

		#placing each card from @hand in an array depending on its suit
		array.each do |card|
			case card.suit
			when 1
				club.push(card)
			when 2
				spade.push(card)
			when 3
				diamond.push(card)
			when 4
				heart.push(card)
			when 5
				trump.push(card)

			end
		end

		#Placing the top of the stacks in an array depending on their suit
		for stack in @stacks
			if !stack.empty?
				case stack.first.suit
				when 1
					club.push(stack.first)
				when 2
					spade.push(stack.first)
				when 3
					diamond.push(stack.first)
				when 4
					heart.push(stack.first)
				when 5
					trump.push(stack.first)
				end

			end
		end

		club = club.sort_by(&:number)
		spade = spade.sort_by(&:number)
		heart = heart.sort_by(&:number)
		diamond = diamond.sort_by(&:number)
		trump = trump.sort_by(&:number)

		#1 : Check the oudlers
		temp_trump = []
		trump.each do |card|
			#The world (21) is worth 10
			if card.number == 121
				score += 10
			#The fool (L'Excuse) is worth 7
			elsif card.number == 100
				score += 7
			#The Magician (Le Petit) depends on how many trump protect him
			# Tweaked to adapt to 2 player version (balanced around having a score of 8 if you have a guaranteed trump number superiority)
			elsif card.number == 101
				case trump.length
				when 0..5
					#Worthless !
				when 6..8
					score += 6
				when 8..10
					score += 7
				else
					score += 8
				end
			else
				temp_trump.push(card)
			end
		end

		trump = temp_trump
		#Remove the world / the excuse / the magician from the trump array : suboptimal, but doing it in previous loop will mess with the iteration and skip some cards

		#2 : Check the trump stack value (without oudlers)
		score += trump.length * 2
		i = 0
		trump.each do |card|
			if card.number > 115
				score += 2

				if trump[i + 1] == (card.number + 1)
					score += 1
				elsif trump[i - 1] == (card.number - 1)
					score += 1
				end
			end
			i += 1
		end
		#3 : Check for honors
		queen = false
		king = false
		spade.each do |card|
			if card.number == 11
				score += 1
			end
			if card.number == 12
				score += 2
			end
			if card.number == 13
				queen = true
			end
			if card.number == 14
				king = true
			end
		end
		if queen && king
			score += 10
		elsif king
			score += 6
		elsif queen
			score += 3
		end

		queen = false
		king = false
		diamond.each do |card|
			if card.number == 11
				score += 1
			end
			if card.number == 12
				score += 2
			end
			if card.number == 13
				queen = true
			end
			if card.number == 14
				king = true
			end
		end
		if queen && king
			score += 10
		elsif king
			score += 6
		elsif queen
			score += 3
		end

		queen = false
		king = false
		heart.each do |card|
			if card.number == 11
				score += 1
			end
			if card.number == 12
				score += 2
			end
			if card.number == 13
				queen = true
			end
			if card.number == 14
				king = true
			end
		end
		if queen && king
			score += 10
		elsif king
			score += 6
		elsif queen
			score += 3
		end

		queen = false
		king = false
		club.each do |card|
			if card.number == 11
				score += 1
			end
			if card.number == 12
				score += 2
			end
			if card.number == 13
				queen = true
			end
			if card.number == 14
				king = true
			end
		end
		if queen && king
			score += 10
		elsif king
			score += 6
		elsif queen
			score += 3
		end

		#4 : Length of each suit

		#long
		long = 7 #Original value to 4 : went to 7 since there are only 2 players (must allow the player to force the others to cut)
		if spade.length > long
			score += 5
			score += (spade.length - long) * 2
		end

		if diamond.length > long
			score += 5
			score += (diamond.length - long) * 2
		end

		if heart.length > long
			score += 5
			score += (heart.length - long) * 2
		end

		if club.length > long
			score += 5
			score += (club.length - long) * 2
		end
		#Short : to be considered ONLY for guard WITHOUT / AGAINST the kitty
		if score >= guard
			potential_score = 0
			case spade.length
			when 0
				potential_score += 5
			when 1
				potential_score += 3
			when 2
				potential_score += 1
			end

			case heart.length
			when 0
				potential_score += 5
			when 1
				potential_score += 3
			when 2
				potential_score += 1
			end

			case diamond.length
			when 0
				potential_score += 5
			when 1
				potential_score += 3
			when 2
				potential_score += 1
			end

			case club.length
			when 0
				potential_score += 5
			when 1
				potential_score += 3
			when 2
				potential_score += 1
			end

			#If the potential score allow guard without kitty, then add it to the score
			if (score + potential_score) >= above_guard
				score = score + potential_score
			end
		end

		return score
	end
end

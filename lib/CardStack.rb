
$LOAD_PATH << File.dirname(__FILE__)

require('Card.rb')

class CardStack

    attr_reader :cards

    def initialize()
        @stack=[]
        for suit in (1..4) # adding standard cards
            for x in (1..14)
                @stack.push(Card.new(suit, x))
            end
        end
        for x in (100..121) # adding trumps
            @stack.push(Card.new(Suit::Trump, x))
        end
        @cards = @stack.length
    end

    def mix_cards()
        r = Random.new()
        max = r.rand(100..300)
        i = 0
        while i < max
            cards = [r.rand(0..@cards) - 1, r.rand(0..@cards) - 1]
            @stack[cards.first], @stack[cards.last] = @stack[cards.last], @stack[cards.first] # swap cards
            i += 1
        end
    end

    def show()
        for card in @stack
            print(card.to_s + "\n")
        end
    end

    def distribute()
        if @stack.length
            @cards -= 1
            return @stack.pop()
        end
        return -1
    end

    def card_number()
        return 78
    end

end

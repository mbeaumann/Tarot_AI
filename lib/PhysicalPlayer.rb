
$LOAD_PATH << File.dirname(__FILE__)

require('Player')

class PhysicalPlayer < Player

    def play(opponent_card=nil)
        print_cards

		puts "(GAME) Your opponent plays #{opponent_card.to_s}" if opponent_card != nil
        print "(GAME) Which card do you want to play ?\n\t> "
        card = gets
        if card.to_i <= @hand.length
            return @hand.delete_at(card.to_i - 1)
        else
            return @stacks[card.to_i - @hand.length - 1].delete_at(0)
        end
    end

	def bet()
        print_cards

		puts "(GAME) " + @name + ", do you want to make an auction ? (y/n)"
		print "\t> "
		yes = gets
		if yes == "y\n"
			valid = false
			while !valid
				puts "(GAME) Auction :"
				print "\tSmall - 1\n"
				print "\tGuard - 2\n"
				print "\tGuard without the kitty - 3\n"
				print "\tGuard against the kitty - 4\n\t> "
				bid = gets
				if bid.to_i > 0 && bid.to_i < 5
					valid = true
					return bid.to_i
				end
			end
		end
		return BID_NO_BID
	end

    def give_hand(hand)
		order_Hand(hand)
	end

	def overbid(bid)
		print "(GAME) #{name}, your opponent has made a higher bid then you.\n\t> "
		case bid
		when BID_SMALL
			puts "SMALL BID"
		when BID_GUARD
			puts "SIMPLE GUARD"
		when BID_GUARD_WITHOUT_KITTY
			puts "GUARD WITHOUT THE KITTY"
		when BID_GUARD_AGAINST_KITTY
			puts "BID GUARD AGAINST KITTY"
			return BID_NO_BID # CANNOT MAKE BETTER
		end
		print "(GAME) Do you want to overbid ?\n\t> "
		answer = gets
		if answer == "y\n" || answer == "Y\n"
			overbid = bid
			while overbid <= bid
				puts "(GAME) Overbid :"
				print "\tSmall - 1\n"
				print "\tGuard - 2\n"
				print "\tGuard without the kitty - 3\n"
				print "\tGuard against the kitty - 4\n\t> "
				new_bid = gets
				if new_bid.to_i > 0 && new_bid.to_i < 5
					overbid = new_bid.to_i
				end
			end
			return overbid
		end
		return BID_NO_BID
	end

	def set_kitty(kitty, bid)
		if bid == BID_SMALL || bid == BID_GUARD
			for i in 0..(kitty.length - 1)
				print_hand(1)
				print "(GAME) Put " + kitty[i].to_s + " in your hand ? (y/n)\n\t> "
				yes = gets
				if yes == "y\n"
					print "(GAME) Enter the number of the card you want to swap with the " + kitty[i].to_s + ": \n\t>  "
					card = gets
					@hand[card.to_i - 1] = kitty[i]
					tmp = @hand[card.to_i - 1]
					kitty[i] = tmp
					order_Hand(@hand)
				end
			end
		else
			add_trick(kitty)
		end
	end

end

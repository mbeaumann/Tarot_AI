
$LOAD_PATH << File.dirname(__FILE__)
File.join(File.dirname(__FILE__), '../')

require('NeuralNetwork.rb')
require('Trainer.rb')

require "fileutils"

class Specie

	attr_reader :neuralNetworkPool

	def initialize(max_allowed_networks=64)
		@neuralNetworkPool = []
		@max_allowed_networks = max_allowed_networks
		@pCrossover = 0.0
		@pNewNodeMutation = 0.4
		@pNewLinkMutation = 0.6
		@pRemoveNodeMutation = 0.5
		@pRemoveLinkMutation = 0.6
		@pChangeWeightMutation = 1.0

		@scores = [] # Used to remind the score of each network during selection process (nb. of won games)
		@played = [] # Used to remind the plyed games of each networks during selection process
	end

	def dump(dump_dir, id, generation)
		generation_dir = "#{dump_dir}/generation_#{generation}"
		current_specie = "#{generation_dir}/specie_#{id}"
		Dir.mkdir(dump_dir) unless File.exists?(dump_dir)
		Dir.mkdir(generation_dir) unless File.exists?(generation_dir)
		Dir.mkdir(current_specie) unless File.exists?(current_specie)
		i = 1
		@neuralNetworkPool.each do |network|
			network.dump(current_specie + "/network_#{i}")
			i += 1
		end
	end

	def load(dump_dir)
		if !File.exists?(dump_dir)
			puts "LoadSpecie : File doesn't exists..."
			return
		end
		Dir.open(dump_dir).each do |network|
			next if network == "." || network == ".."
			loaded = NeuralNetwork.new
			loaded.load(dump_dir + "/#{network}")
			@neuralNetworkPool << loaded
		end
	end

	def addNeuralNetwork(neuralNetwork)
		@neuralNetworkPool << neuralNetwork
	end

	def copyNeuralNetwork(oldNetwork)
		newNetwork = NeuralNetwork.new()

		oldNetwork.nodes.values.each do |node|
			newNetwork.add_node(Node.new(node.id, node.value, node.type))
		end
		oldNetwork.links.values.each do |link_list|
			link_list.each do |link|
				nodeInput = newNetwork.getNodeById(link.nodeInput.id)
				nodeOutput = newNetwork.getNodeById(link.nodeOutput.id)
				newNetwork.add_link(Link.new(nodeInput, nodeOutput, link.weight, link.innovation))
			end
		end
		return newNetwork
	end

	def get_explicit_fitness_sharing()
		explicit_fitness_sharing = 0.0
		population = @neuralNetworkPool.size

		@scores.each_index do |i|
			fitness = @scores[i].to_f / @played[i].to_f # (nb. win / nb. party played)
			adjusted_fitness = fitness.to_f / population.to_f
			explicit_fitness_sharing += adjusted_fitness
		end

		return explicit_fitness_sharing
	end

	def crossover(representative1, representative2)
		result = NeuralNetwork.new

		i = 0
		representative1_links = representative1.get_links_array
		representative2_links = representative2.get_links_array

		max = representative2_links.size
		representative1_links.each do |link|
			while i < max && link.innovation > representative2_links[i].innovation
				crossover_add_link_to(result, representative2_links[i])
				i += 1
			end

			if i < max
				if link.innovation == representative2_links[i].innovation
					crossover_add_link_to(result, link)
					if !link.activated || !representative2_links[i].activated
						link.activated = false
					end
					i += 1
				else
					crossover_add_link_to(result, link)
				end
			else
				crossover_add_link_to(result, link)
			end
		end

		while i < max
			crossover_add_link_to(result, representative2_links[i])
			i += 1
		end

		return result
	end

	# Crossover utility function. Add a node to a network and check if the input and output node of the link are
	# present in the network and add them if not
	def crossover_add_link_to(network, link)
		if network.nodes[link.nodeInput.id] == nil
			network.add_node(link.nodeInput)
		end
		if network.nodes[link.nodeOutput.id] == nil
			network.add_node(link.nodeOutput)
		end
		network.add_link(link)
	end

	# Use the remaining networks to create mutated networks. it takes 2 random existing networks to cross them over.
	# The freshly created network undergoes a mutation.
	def make_mutations(innovation, offsprings)
		rng = Random.new

		explicit_fitness_sharing = get_explicit_fitness_sharing()
		@neuralNetworkPool.each_with_index do |network, i| # Deleting the weakest networks
			if (@scores[i].to_f / @played[i].to_f) < explicit_fitness_sharing
				@neuralNetworkPool.delete_at(i) if @neuralNetworkPool.size > 2 # At least 2 networks should subsist
			end
		end

		population = @neuralNetworkPool.size
		new_networks = []
		offsprings.times do
			network = self.getRepresentative

			p = rng.rand(1.0)
			if p < @pCrossover
				idx1 = rng.rand(population)
				begin
					idx2 = rng.rand(population)
				end while idx1 == idx2
				crossed_network = crossover(@neuralNetworkPool[idx1], @neuralNetworkPool[idx2])
				network = copyNeuralNetwork(crossed_network)
			elsif p < @pNewNodeMutation
				newNodeMutation(network, innovation)
			elsif p < @pNewLinkMutation
				newLinkMutation(network, innovation)
			# elsif p < @pRemoveNodeMutation
			# 	removeNodeMutation(network)
			# elsif p < @pRemoveLinkMutation
			# 	removeLinkMutation(network)
			elsif p < @pChangeWeightMutation
				changeWeightMutation(network)
			end

			new_networks << network
			innovation += 1
		end

		return new_networks
	end

	def newNodeMutation(network, innovation)
		# puts "[NewNodeMutation]"
		id = network.nodes.keys.max + 1
		newNode = Node.new(id, 0.0, 0)
		network.add_node(newNode)

		link = network.get_random_link

		link.activated = false
		input = link.nodeInput
		output = link.nodeOutput
		weight = link.weight
		network.add_link(Link.new(input, newNode, weight, innovation))
		network.add_link(Link.new(newNode, output, weight, innovation))
	end

	def newLinkMutation(network, innovation)
		# puts "[NewLinkMutation]"
		node1 = network.get_random_node
		node2 = network.get_random_node
		network.add_link(Link.new(node1, node2, 1, innovation))
	end

	def changeWeightMutation(network)
		# puts "[changeWeightMutation]"
		rng = Random.new()
		network.get_random_link().weight = rng.rand(1.0)
	end

	#Will try to remove a node and its link : return 0 on success, if roll for a input or output node, return 1 and do nothing
	def removeNodeMutation(network)
		begin
			node = network.get_random_node
			res = network.remove_node(node)
		end while res == nil
	end

	def removeLinkMutation(network)
		link = network.get_random_link
		network.remove_link(link)
	end

	# Should return a parent for crossover
	def getRepresentative()
		return @neuralNetworkPool[rand(@neuralNetworkPool.size)]
	end

	def select()
		rng = Random.new

		population = @neuralNetworkPool.size
		@scores = Array.new(population, 0)
		@played = Array.new(population, 0)

		# Each network plays 10 matches against random opponent. The winner win 1 point
		threads = []
		@neuralNetworkPool.each_with_index do |network, i|
			threads << Thread.new {
				5.times do
					begin
						opponent = rng.rand(population)
					end while opponent == i

					trainer = Trainer.new
					winner = trainer.game(network, @neuralNetworkPool[opponent])
					if winner == network
						@scores[i] += 1
					else
						@scores[opponent] += 1
					end

					@played[i] += 1
					@played[opponent] += 1
				end
			}
		end
		threads.each { |t| t.join }
	end
end

# s1 = Specie.new
# s1.load(1, 902)
#
# first = s1.neuralNetworkPool.first
# puts "*********************************************\nNetwork nodes : #{first.nodes.length} - Netwwork links : #{first.links.length}\n"
#
# s1.select


$LOAD_PATH << File.dirname(__FILE__)

require('Player')

class AIPlayer < Player

    attr_reader :network

    def initialize(name, network)
        super(name)
        @network = network
    end

    def give_hand(hand)
        order_Hand(hand)
        @hand.each do |card| # Enable nodes corresponding to the hand cards of the player
            @network.updateNode(78 + get_corresponding_node_idx(card), 1.0)
        end
        @stacks.each do |stack|
            @network.updateNode(78 + get_corresponding_node_idx(stack[0]), 1.0)
        end
    end

    def play(opponent_played_card=nil)
        @network.updateNode((2 * 78) + get_corresponding_node_idx(opponent_played_card), 1.0) if opponent_played_card != nil

        max = 0
        card_to_play = nil
        (1..78).each do |output_id|
            current_card = get_card_from_idx(output_id - 1)
            if @network.nodes[output_id].value > max && has_card(current_card)
                if opponent_played_card != nil && have_suit(opponent_played_card.suit) && current_card.suit != opponent_played_card.suit
                    next
                end
                card_to_play = current_card
                max = @network.nodes[output_id].value
            end
        end

		return nil if card_to_play == nil # found no card to play 

        @network.updateNode((2 * 78) + get_corresponding_node_idx(opponent_played_card), 0.0) if opponent_played_card != nil #reset
        @network.updateNode(78 + get_corresponding_node_idx(card_to_play), 0.0)
        return remove_card(card_to_play)
    end

    def get_corresponding_node_idx(card)
        if card.suit == 5 # Trump
            return (14 * (card.suit - 1)) + (card.number - 99) #(card.suit * (card.number - 99))
        end
        return (14 * (card.suit - 1)) + card.number #(card.suit * card.number)
    end

    def get_card_from_idx(idx)
        if idx >= 56  #trump
            return Card.new(5, (idx - 56) + 100)
        end
        card_suit = ((idx - (idx % 14)) / 14) + 1
        return Card.new(card_suit, (idx % 14) + 1)
    end

    def bet() # Made by Clem in waiting for that from Victor
        # 40pts : Small Bid
        # 56pts : Guard
        # 71pts : Guard without kitty
        # 81pts : Guard against kitty
        score = hand_value
        case score
        # when 0..39
        #     return BID_NO_BID
        when 0..55
            return BID_SMALL
        when 56..70
            return BID_GUARD
        when 71..80
            return BID_GUARD_WITHOUT_KITTY
        else
            return BID_GUARD_AGAINST_KITTY
        end
        return BID_NO_BID
    end

    def overbid(bid) #Never overbid for now
        return BID_NO_BID
    end

    def set_kitty(kitty, bid)
        # if bid == BID_SMALL || bid == BID_GUARD
        #     # hand_length = @hand.length
        #     max_value = hand_value
        #     max_hand = @hand.clone
        #     for i in 1..(kitty.length - 1)
        #         curr_kitty = kitty.clone
        #         comb = curr_kitty.combination(i).to_a
        #         for to_swap in comb
        #             indexes = Array.new(to_swap.length, 0)
        #             while true
        #                 break if inc(indexes, 0) == -1
        #                 curr_hand = swap_hand(@hand.clone, indexes, to_swap)
        #                 value = hand_value(curr_hand)
        #                 if value > max_value then
        #                     max_value = value
        #                     max_hand = curr_hand
        #                 end
        #             end
        #         end
        #     end
        #     kitty = (@hand.concat kitty) - max_hand
        #     @hand = max_hand
        #     order_Hand(@hand)
        # end
        add_trick(kitty)
    end

    def inc(indexes, index)
        value = indexes[index] + 1
        if value % @hand.length == 0
            if index == indexes.length - 1
                return -1
            end
            if inc(indexes, index + 1) == -1
                return -1
            end
            indexes[index] = indexes[index + 1]
            inc(indexes, index)
        else
            indexes[index] = value
        end
        return 0
    end

    def swap_hand(hand, indexes, kitty)
        for i in 0..indexes.length - 1
            hand[indexes[i]] = kitty[i]
        end
        return hand
    end

    def to_s
        return "Type : AIPlayer\n" + super
    end
end


$LOAD_PATH << File.dirname(__FILE__)
File.join(File.dirname(__FILE__), '../')

require('Darwin.rb')

# This script aim to make games between neural network of a certain specie to see the result.

darwin = Darwin.new
darwin.load_all

trainer = Trainer.new

darwin.species.each do |specie|
    n1 = specie.getRepresentative
    n2 = specie.getRepresentative

    puts "1) #{n1.get_links_array.size} links - #{n1.nodes.keys.size} nodes"
    puts "2) #{n2.get_links_array.size} links - #{n2.nodes.keys.size} nodes"

    trainer.game(n1, n2)
    puts "Next"
    gets
end

# darwin = Darwin.new
# darwin.load_all("dump/results")
#
# trainer = Trainer.new
#
# running = true
# while running
#     n1 = darwin.species.first.getRepresentative
#     n2 = darwin.species.first.getRepresentative
#     puts "1) #{n1.get_links_array.size} links - #{n1.nodes.keys.size} nodes"
#     puts "2) #{n2.get_links_array.size} links - #{n2.nodes.keys.size} nodes"
#     trainer.game(n1, n2)
#     puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"#"\n\t> Save them ? "
#     gets
# end

class Node

	attr_reader :id
	attr_reader :type
	attr_accessor :value

	def initialize(id, value, type)
		@id = id #An integer to identify the neuron
		@value = value #Float (0.0 <= value <= 1.0)
		@type = type #See Type enum
	end

	def to_hash #returns a hashtable representing the node, used after to convert to JSON
		hash = {}
		instance_variables.each {|var| hash[var.to_s.delete("@")] = instance_variable_get(var) }
		return hash
	end

	def self.createFromHash(hash)
		id = hash.dig("id")
		value = hash.dig("value")
		type = hash.dig("type")
		node = Node.new(id, value, type)
		return node
	end

	def equalTest(other)
		if @id == other.id && @type == other.type && @value == other.value
			# puts "node.equals returned true"
			return true
		end
		# puts "node.equals returned false"
		return false
	end

end

# Enumeration classes
class Type
	Hidden=0
	Input=1
	Output=2

	def self.stringify(num)
		case num
		when 0
			"Hidden"
		when 1
			"Input"
		when 2
			"Output"
		else
			"Unknown type"
		end
	end
end

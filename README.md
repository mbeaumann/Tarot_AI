===== AI Tutored project on French tarot (2 player variation) ===== 

Made as part of the first year of the Master's degree course at the UBFC (Université de Bourgogne/Franche-Comté)

=== Requirements ===

- A working JVM

=== Project Layout ===

- ./bin : Binary files required (JRuby interpreter)
- ./lib : Main .rb files
- ./test : Test related .rb files
- ./resources : external files required (pictures)

=== List of gems used ===

If you do not want to use the interpreter in the repository, make sure you have installed the following gems :

- jrubyfx

LIBDIR = ./lib
TESTDIR = ./test
JRUBY = ./bin/jruby/bin/jruby
JIRB = ./bin/jruby/bin/jirb
.PHONY: run test

run:
	$(JIRB) -r $(LIBDIR)/Tarot.rb

test:
	$(JRUBY) $(TESTDIR)/run_tests.rb

games:
	$(JRUBY) $(LIBDIR)/Test.rb

darwin:
	$(JIRB) -r $(LIBDIR)/Darwin.rb

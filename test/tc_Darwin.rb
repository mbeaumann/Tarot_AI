require_relative "../lib/Darwin.rb"
require_relative "../lib/NeuralNetwork.rb"
require_relative "../lib/Specie.rb"
require_relative "../lib/Node.rb"
require_relative "../lib/Link.rb"

require "test/unit"

class TestLifeCreation < Test::Unit::TestCase

    def test_simple
        darwin = Darwin.new
        first  = darwin.species[0].neuralNetworkPool[0]

        assert(first.nodes.length == 78 * 3, "TestLifeCreation : Wrong number of loaded nodes... Expected #{3 * 78}, got #{first.nodes.length}")
        first.nodes.each do |key, value|
            if key > 78 && key <= 156
                assert(first.links[key].length == 1, "TestLifeCreation : Each Node (exept output nodes) should have one link connected to it... Expected 1, got #{first.links[key].length}")
                first.links[key].each do |link|
                    assert((link.nodeInput.id - link.nodeOutput.id) % 78 == 0, "TestLoadAll : Wrong link connections")
                end
            elsif key > 156
                if (key - 1) % 78 < 56
                    assert(first.links[key].length == 14, "TestLifeCreation : Expecting 14 links starting from node #{key}, got #{first.links[key].length}")
                # else
                #     assert(first.links[key].length == 22, "TestLifeCreation : Expecting 22 links starting from node #{key}, got #{first.links[key].length}")
                end
            end
        end

        assert(darwin.innovation == 2, "TestLifeCreation : Innovation number should be equals to TWO after creation of the first network")
    end
end

class TestDumpAll < Test::Unit::TestCase

    def test_simple
        File.join(File.dirname(__FILE__), '../')
        darwin = Darwin.new
        darwin.dump_all("test_dump")

        assert(File.exists?("test_dump/darwin"), "TestDumpAll : File \"darwin\" doesn't exists...")
        assert(File.exists?("test_dump/generation_1/specie_1/network_1"), "TestDumpAll : First specie hasn't been dump...")

        file = File.new("test_dump/darwin", "r") # Load last innovation number
		innovation = file.gets.to_i
		file.close

        assert(innovation == 2, "TestDumpAll : Wrong innovation number in \"darwin\" file")
        FileUtils.rm_rf("test_dump")
    end
end

class TestLoadAll < Test::Unit::TestCase

    def test_simple
        File.join(File.dirname(__FILE__), '../')

        darwin = Darwin.new
        darwin.dump_all("test_dump")

        assert(File.exists?("test_dump/darwin"), "TestDumpAll : File \"darwin\" doesn't exists...")

        darwin.load_all("test_dump")
        first  = darwin.species[0].neuralNetworkPool[0]

        assert(first.nodes.length == 78 * 3, "TestLifeCreation : Wrong number of loaded nodes... Expected #{3 * 78}, got #{first.nodes.length}")
        first.nodes.each do |key, value|
            if key > 78 && key <= 156
                assert(first.links[key].length == 1, "TestLifeCreation : Each Node (exept output nodes) should have one link connected to it... Expected 1, got #{first.links[key].length}")
                first.links[key].each do |link|
                    assert((link.nodeInput.id - link.nodeOutput.id) % 78 == 0, "TestLoadAll : Wrong link connections")
                end
            elsif key > 156
                if (key - 1) % 78 < 56
                    assert(first.links[key].length == 14, "TestLifeCreation : Expecting 14 links starting from node #{key}, got #{first.links[key].length}")
                # else
                #     assert(first.links[key].length == 22, "TestLifeCreation : Expecting 22 links starting from node #{key}, got #{first.links[key].length}")
                end
            end
        end

        # (56..78).each do |id|
        #     assert(first.links[2 * 78 + id].size == (78 - id - 1), "TestLifeCreation : Node #{id} should have #{78 - id - 1} exiting connections, got #{first.links[2 * 78 + id].size}")
        # end

        assert(darwin.innovation == 2, "TestLoadAll : Innovation number should be equals to 3 after loading")
        FileUtils.rm_rf("test_dump")
    end
end

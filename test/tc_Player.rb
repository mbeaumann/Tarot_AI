require_relative "../lib/Player.rb"
require_relative "../lib/AIPlayer.rb"
require_relative "../lib/CardStack.rb"

require "test/unit"

class TestHandEvaluation < Test::Unit::TestCase

	def test_simple
		p = Player.new("rmekarni")
		cs = CardStack.new()
		cs.mix_cards()
		hand = Array.new(20) { cs.distribute() }
		stacks = Array.new(4) { Array.new(4) { cs.distribute() }}
		p.give_hand(hand)
		p.give_stacks(stacks)

		assert(p.hand_value() >= 0, "hand_value has a negative score")
	end
end

class TestOrderHand < Test::Unit::TestCase
	def test_simple
		p = Player.new("rmekarni")
		cs = CardStack.new()
		cs.mix_cards()
		hand = Array.new(20) { cs.distribute() }
		p.give_hand(hand)
		for i in 1..19 do
			if(p.hand[i - 1].suit == p.hand[i].suit)
				assert(p.hand[i-1].number < p.hand[i].number, "Test order hand : hand is not ordered")
			end
		end
	end

end

class PrintHand < Test::Unit::TestCase
	def test_simple
		p = Player.new("rmekarni")
		cs = CardStack.new()
		cs.mix_cards()
		hand = Array.new(20) { cs.distribute}
		p.give_hand(hand)
		begin
			p.print_hand(1)
		rescue => coin
			assert(false, "Test : Player.print_hand failed " + coin.to_s)
		end
	end
end

class PrintStack < Test::Unit::TestCase
	def test_simple
		p = Player.new("rmekarni")
		cs = CardStack.new()
		cs.mix_cards()
		stacks1 = Array.new(4) { Array.new(4) { cs.distribute() }}
		p.give_stacks(stacks1)
		begin
			p.print_stacks(1)
		rescue => coin
			assert(false, "Test : Player.print_stacks failed " + coin.to_s)
		end
	end
end


class HandValue21AndFoolTest < Test::Unit::TestCase
	def test_simple
		p = Player.new("rmekarni")
		customHand = Array.new()
		world = Card.new(5,121)
		excuse = Card.new(5,100)
		customHand.push(world)
		customHand.push(excuse)
		p.give_hand(customHand)
		res = p.hand_value()
		assert(res == 17, "Test : Player.hand_value : 21 and The Fool : failed, result should be 17 but is " + res.to_s)

	end
end

class HandValueSmallTest < Test::Unit::TestCase
	def test_simple
		p = Player.new("rmekarni")
		customHand = Array.new()
		petit = Card.new(5,101)
		customHand.push(petit)
		customHand.push(Card.new(5,106))
		customHand.push(Card.new(5,107))
		customHand.push(Card.new(5,108))
		customHand.push(Card.new(5,109))
		customHand.push(Card.new(5,110))
		customHand.push(Card.new(5,111))
		customHand.push(Card.new(5,112))
		customHand.push(Card.new(5,113))
		p.give_hand(customHand)
		res = p.hand_value()
		assert(res == 23,"Test handValue petit 9th : score should be 23 but is " + res.to_s)
	end
end

class HandValueTestColor < Test::Unit::TestCase
	def test_simple
		p = Player.new("rmekarni")
		customHand = Array.new()
		customHand.push(Card.new(2, 11))
		customHand.push(Card.new(2, 12))
		customHand.push(Card.new(2, 13))
		customHand.push(Card.new(2, 14))
		p.give_hand(customHand)
		res = p.hand_value()
		assert(res == 13,"Test handValue 11,12,13,14: score should be 13 but is " + res.to_s)
	end
end

#=begin
class HandValueTestColorLong < Test::Unit::TestCase
	def test_simple
		p = Player.new("rmekarni")
		customHand = Array.new()
		customHand.push(Card.new(2, 5))
		customHand.push(Card.new(2, 6))
		customHand.push(Card.new(2, 7))
		customHand.push(Card.new(2, 8))
		customHand.push(Card.new(2, 9))
		customHand.push(Card.new(2, 10))
		customHand.push(Card.new(2, 11))
		customHand.push(Card.new(2, 12))
		customHand.push(Card.new(2, 13))
		customHand.push(Card.new(2, 14))
		p.give_hand(customHand)
		res = p.hand_value()
		assert(res == 24,"Test handValue long: score should be 24 but is " + res.to_s)
	end
end
#=end

class HandValueGuradWithout < Test::Unit::TestCase
	def test_simple
		p = Player.new("rmekarni")
		customHand = Array.new()
		customHand.push(Card.new(5, 121))
		customHand.push(Card.new(5, 100)) #17

		customHand.push(Card.new(2, 5))
		customHand.push(Card.new(2, 6))
		customHand.push(Card.new(2, 7))
		customHand.push(Card.new(2, 8))
		customHand.push(Card.new(2, 9))
		customHand.push(Card.new(2, 10))
		customHand.push(Card.new(2, 11))
		customHand.push(Card.new(2, 12))
		customHand.push(Card.new(2, 13))
		customHand.push(Card.new(2, 14)) #41

		customHand.push(Card.new(1, 11))
		customHand.push(Card.new(1, 12))
		customHand.push(Card.new(1, 13))
		customHand.push(Card.new(1, 14)) #41 + 13 = 54

		customHand.push(Card.new(3, 11))
		customHand.push(Card.new(3, 12))
		customHand.push(Card.new(3, 13))
		customHand.push(Card.new(3, 14)) # 54 + 13 = 67
		#+ potential score thing : 72
		p.give_hand(customHand)
		res = p.hand_value()
		assert(res == 72,"Test handValue guardWithout/All : score should be 72 but is " + res.to_s)
	end
end

class RemoveCardTest < Test::Unit::TestCase

	def test_simple
		p = Player.new("rmekarni")
		customHand = Array.new()
		customHand.push(Card.new(5, 121))
		customHand.push(Card.new(5, 100))
		customHand.push(Card.new(2, 5))
		customHand.push(Card.new(2, 6))
		p.give_hand(customHand)

		s = [[Card.new(2, 7)], [], [], []]
		p.give_stacks(s)

		card = p.remove_card(Card.new(2, 6))
		assert(card != nil && card.suit == 2 && card.number == 6, "RemoveCardTest : wrong card removed : #{card}")
		p.hand.each { |c| if c.suit == card.suit then assert(c.number != card.number, "RemoveCardTest : card not removed : #{c}") end }

		card = p.remove_card(Card.new(2, 5))
		assert(card != nil && card.suit == 2 && card.number == 5, "RemoveCardTest : wrong card removed : #{card}")
		p.hand.each { |c| if c.suit == card.suit then assert(c.number != card.number, "RemoveCardTest : card not removed : #{c}") end }

		card = p.remove_card(Card.new(5, 100))
		assert(card != nil && card.suit == 5 && card.number == 100, "RemoveCardTest : wrong card removed : #{card}")
		p.hand.each { |c| if c.suit == card.suit then assert(c.number != card.number, "RemoveCardTest : card not removed : #{c}") end }

		card = p.remove_card(Card.new(5, 121))
		assert(card != nil && card.suit == 5 && card.number == 121, "RemoveCardTest : wrong card removed : #{card}")
		p.hand.each { |c| if c.suit == card.suit then assert(c.number != card.number, "RemoveCardTest : card not removed : #{c}") end }
		assert(p.hand.size == 0, "RemoveCardTest : hand should be empty")

		card = p.remove_card(Card.new(2, 7))
		assert(card != nil && card.suit == 2 && card.number == 7, "RemoveCardTest : wrong card removed : #{card}")
		p.stacks.each { |s| assert(s.empty?, "RemoveCardTest : stack sould be empty") }
	end
end

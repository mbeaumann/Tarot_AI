
require_relative "../lib/Darwin.rb"
require_relative "../lib/NeuralNetwork.rb"
require_relative "../lib/Specie.rb"
require_relative "../lib/Node.rb"
require_relative "../lib/Link.rb"

require "test/unit"
require "fileutils"

class TestSelect < Test::Unit::TestCase

    def test_simple()
        darwin = Darwin.new
        darwin.species.first.neuralNetworkPool << darwin.create_life
        darwin.species.first.select

        # assert(darwin.species.first.neuralNetworkPool.length == 1, "TestSelect : Only one network should subsist after this selection")
    end
end

class TestDump < Test::Unit::TestCase

    def test_simple()
        File.join(File.dirname(__FILE__), '../')

        darwin = Darwin.new

        darwin.species[0].dump("test_dump", 1, 1)
        assert(File.exists?("test_dump/generation_1/specie_1/network_1"), "TestDump : Dump doesn't work")
        FileUtils.rm_rf("test_dump")
    end
end

class TestLoad < Test::Unit::TestCase

    def test_simple()
        File.join(File.dirname(__FILE__), '../')

        # darwin = Darwin.new
        s = Specie.new
        network = NeuralNetwork.new()
        node1 = Node.new(1, 0.0, 1)
        node2 = Node.new(2, 0.0, 2)
        node3 = Node.new(3, 0.0, 0)
        node4 = Node.new(12, 0.0, 1)
        network.add_node(node1)
        network.add_node(node2)
        network.add_node(node3)
        network.add_node(node4)
        network.add_link(Link.new(node1, node3, 1, 1))
        network.add_link(Link.new(node4, node3, 0.5, 1))
        network.add_link(Link.new(node3, node2, 1, 1))
        s.addNeuralNetwork(network)
        s.dump("test_dump", 1, 1)

        toLoad = Specie.new
        toLoad.load("test_dump/generation_1/specie_1")
        loaded = toLoad.neuralNetworkPool[0]

        assert(loaded.nodes.length == 4, "TestDump : Wrong number of loaded nodes")
        assert(loaded.links.length == 3, "TestDump : Wrong number of loaded links")

        assert(loaded.nodes[1].id == 1 && loaded.nodes[1].type == 1, "TestDump : Wrong node idx or type")
        assert(loaded.nodes[2].id == 2 && loaded.nodes[2].type == 2, "TestDump : Wrong node idx or type")
        assert(loaded.nodes[3].id == 3 && loaded.nodes[3].type == 0, "TestDump : Wrong node idx or type")
        assert(loaded.nodes[12].id == 12 && loaded.nodes[12].type == 1, "TestDump : Wrong node idx or type")

        link_number = 0
        network.links.values.each do |link_list|
            link_number += link_list.length
        end
        assert(link_number == 3, "TestDump : wrong number of link loaded... Expected 3, got #{link_number}")

        assert(loaded.links[1][0].nodeInput.id == 1 && loaded.links[1][0].nodeOutput.id == 3, "TestDump : Wrong input or output node for loaded link")
        assert(loaded.links[12][0].nodeInput.id == 12 && loaded.links[12][0].nodeOutput.id == 3, "TestDump : Wrong input or output node for loaded link")
        assert(loaded.links[3][0].nodeInput.id == 3 && loaded.links[3][0].nodeOutput.id == 2, "TestDump : Wrong input or output node for loaded link")

        FileUtils.rm_rf("test_dump")
    end
end

class TestNewNodeMutations < Test::Unit::TestCase

    def test_simple
        darwin = Darwin.new
        specie = darwin.species.first
        network = specie.neuralNetworkPool.first

        old_node_number = network.nodes.length
        old_link_number = 0
        network.links.values.each do |link_list|
            old_link_number += link_list.length
        end

        specie.newNodeMutation(network, darwin.innovation)

        link_number = 0
        network.links.values.each do |link_list|
            link_number += link_list.length
        end

        assert(network.nodes.length == old_node_number + 1, "TestNewNodeMutations : The mutation doesn't add a new node")
        assert(link_number == old_link_number + 2, "TestNewNodeMutations : The mutation doesn't add two new link")

        # network.links.each do |link|
        #     assert(link.nodeInput.class == Node, "TestNewNodeMutations : Link tranformed ...")
        #     assert(link.nodeOutput.class == Node, "TestNewNodeMutations : Link tranformed ...")
        # end
    end
end

class TestNewLinkMutations < Test::Unit::TestCase

    def test_simple
        darwin = Darwin.new
        specie = darwin.species.first
        network = specie.neuralNetworkPool.first

        old_node_number = network.nodes.length
        old_link_number = 0
        network.links.values.each do |link_list|
            old_link_number += link_list.length
        end
        specie.newLinkMutation(network, darwin.innovation)

        assert(network.nodes.length == old_node_number, "TestNewLinkMutations : The mutation add or remove a new node")
        link_number = 0
        network.links.values.each do |link_list|
            link_number += link_list.length
        end
        assert(link_number == old_link_number + 1, "TestNewLinkMutations : The mutation doesn't add a new link")

        # network.links.each do |link|
        #     assert(link.nodeInput.class == Node, "TestNewLinkMutations : Link tranformed ...")
        #     assert(link.nodeOutput.class == Node, "TestNewLinkMutations : Link tranformed ...")
        # end
    end
end

class TestChangeWeightMutations < Test::Unit::TestCase

    def test_simple
        darwin = Darwin.new
        specie = darwin.species.first
        network = specie.neuralNetworkPool.first

        old_network = specie.copyNeuralNetwork(network)
        specie.changeWeightMutation(network)

        assert(network.nodes.length == old_network.nodes.length, "TestNewLinkMutations : Nodes changed")
        assert(network.links.length == old_network.links.length, "TestNewLinkMutations : The mutation add a new link")

        changed_links = 0
        network.links.each do |key, link_list|
            link_list.each_with_index do |link, i|
                old_link = old_network.links[key][i]
                if link.nodeInput.id == old_link.nodeInput.id && link.nodeOutput.id == old_link.nodeOutput.id &&
                link.innovation == old_link.innovation && link.weight != old_link.weight
                    changed_links += 1
                end
            end
        end

        assert(changed_links == 1, "TestNewLinkMutations : Mutation should change only on link weight... #{changed_links} links changed")

        # network.links.each do |link|
        #     assert(link.nodeInput.class == Node, "TestNewLinkMutations : Link tranformed ...")
        #     assert(link.nodeOutput.class == Node, "TestNewLinkMutations : Link tranformed ...")
        # end
    end
end

class TestRemoveNodeMutations < Test::Unit::TestCase

    def test_simple
        darwin = Darwin.new
        specie = darwin.species.first
        network = specie.neuralNetworkPool.first
        specie.newNodeMutation(network, darwin.innovation) # add a hidden node
        old_network = specie.copyNeuralNetwork(network)

        begin
            res = specie.removeNodeMutation(network)
        end while res == 1

        assert(network.nodes.length == old_network.nodes.length - 1, "TestRemoveNodeMutations : Node not removed")

        removed_node = nil
        old_network.nodes.values.each do |node|
            res = network.getNodeById(node.id)
            if res == nil
                removed_node = node.id
                break
            end
        end

        # network.links.each do |link|
        #     assert(link.nodeInput.class == Node, "TestNewLinkMutations : Link tranformed ...")
        #     assert(link.nodeOutput.class == Node, "TestNewLinkMutations : Link tranformed ...")
        # end

        network.links.values.each do |link_list|
            link_list.each do |link|
                assert(link.nodeInput.id != removed_node, "TestRemoveNodeMutations : link connect to removed node is still here")
                assert(link.nodeOutput.id != removed_node, "TestRemoveNodeMutations : link connect to removed node is still here")
            end
        end
    end
end

class TestRemoveLinkMutation < Test::Unit::TestCase

    def test_simple
        darwin = Darwin.new
        specie = darwin.species.first
        network = specie.neuralNetworkPool.first

        old_network = specie.copyNeuralNetwork(network)
        old_link_number = 0
        old_network.links.values.each do |link_list|
            old_link_number += link_list.length
        end

        specie.removeLinkMutation(network)

        link_number = 0
        network.links.values.each do |link_list|
            link_number += link_list.length
        end

        assert(network.nodes.length == old_network.nodes.length, "TestNewLinkMutations : Nodes changed")
        assert(link_number == old_link_number - 1, "TestNewLinkMutations : The mutation doesn't removes link")

        # network.links.each do |link|
        #     assert(link.nodeInput.class == Node, "TestNewLinkMutations : Link tranformed ...")
        #     assert(link.nodeOutput.class == Node, "TestNewLinkMutations : Link tranformed ...")
        # end
    end
end

class TestCrossover < Test::Unit::TestCase

    def test_simple()
        s1 = Specie.new
        #Creation of the 2 neural networks from page 109 of http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf
        n1 = NeuralNetwork.new
        n1_nodeInput1 = Node.new(1, 0.0, 1)
        n1_nodeInput2 = Node.new(2, 0.0, 1)
        n1_nodeInput3 = Node.new(3, 0.0, 1)
        n1_nodeOutput = Node.new(4, 0.0, 2)
        n1_nodeHidden = Node.new(5, 0.0, 0)
        n1.add_node(n1_nodeInput1)
        n1.add_node(n1_nodeInput2)
        n1.add_node(n1_nodeInput3)
        n1.add_node(n1_nodeOutput)
        n1.add_node(n1_nodeHidden)
        n1.add_link(Link.new(n1_nodeInput1, n1_nodeOutput, 0.0, 1))
        slink = Link.new(n1_nodeInput2, n1_nodeOutput, 0.0, 2)
        slink.activated = false
        n1.add_link(slink)
        n1.add_link(Link.new(n1_nodeInput3, n1_nodeOutput, 0.0, 3))
        n1.add_link(Link.new(n1_nodeInput2, n1_nodeHidden, 0.0, 4))
        n1.add_link(Link.new(n1_nodeHidden, n1_nodeOutput, 0.0, 5))
        n1.add_link(Link.new(n1_nodeInput1, n1_nodeHidden, 0.0, 8))
        s1.neuralNetworkPool.push(n1)

        n2 = NeuralNetwork.new
        n2_nodeInput1 = Node.new(1, 0.0, 1)
        n2_nodeInput2 = Node.new(2, 0.0, 1)
        n2_nodeInput3 = Node.new(3, 0.0, 1)
        n2_nodeOutput = Node.new(4, 0.0, 2)
        n2_nodeHidden1 = Node.new(5, 0.0, 0)
        n2_nodeHidden2 = Node.new(6, 0.0, 0)
        n2.add_node(n2_nodeInput1)
        n2.add_node(n2_nodeInput2)
        n2.add_node(n2_nodeInput3)
        n2.add_node(n2_nodeOutput)
        n2.add_node(n2_nodeHidden1)
        n2.add_node(n2_nodeHidden2)
        n2.add_link(Link.new(n2_nodeInput1, n2_nodeOutput, 0.0, 1))
        slink = Link.new(n2_nodeInput2, n2_nodeOutput, 0.0, 2)
        slink.activated = false
        n2.add_link(slink)
        n2.add_link(Link.new(n2_nodeInput3, n2_nodeOutput, 0.0, 3))
        n2.add_link(Link.new(n2_nodeInput2, n2_nodeHidden1, 0.0, 4))
        slink = Link.new(n2_nodeHidden1, n2_nodeOutput, 0.0, 5)
        slink.activated = false
        n2.add_link(slink)
        n2.add_link(Link.new(n2_nodeHidden1, n2_nodeHidden2, 0.0, 6))
        n2.add_link(Link.new(n2_nodeHidden2, n2_nodeOutput, 0.0, 7))
        n2.add_link(Link.new(n2_nodeInput3, n2_nodeHidden1, 0.0, 9))
        n2.add_link(Link.new(n2_nodeInput1, n2_nodeHidden2, 0.0, 10))
        s1.neuralNetworkPool.push(n2)

        newNetwork = s1.crossover(n1, n2)

        assert(newNetwork.links[1].length == 3, "TestCrossover : Wrong link number connected to node 1")
        assert(newNetwork.links[2].length == 1 + 1, "TestCrossover : Wrong link number connected to node 2") # one link disabled
        assert(newNetwork.links[3].length == 2, "TestCrossover : Wrong link number connected to node 3")
        assert(newNetwork.links[4] == nil, "TestCrossover : No link should exit from node 4")
        assert(newNetwork.links[5].length == 1 + 1, "TestCrossover : Wrong link number connected to node 5") # one link disabled
        assert(newNetwork.links[6].length == 1, "TestCrossover : Wrong link number connected to node 6")
    end
end

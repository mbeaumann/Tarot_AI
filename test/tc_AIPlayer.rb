
require_relative "../lib/AIPlayer.rb"
require_relative "../lib/Card.rb"
require_relative "../lib/Darwin.rb"

require "test/unit"

class TestAIPlayer  < Test::Unit::TestCase

	def test_simple
		darwin = Darwin.new
		player = AIPlayer.new("Network I", darwin.create_life)

		card1 = Card.new(1, 1) # Ace of Club
		card2 = Card.new(2, 4) # Four of Spade
		card3 = Card.new(3, 9) # Nine of Diamond
		card4 = Card.new(4, 14) # King of Heart
		card5 = Card.new(5, 100) # The fool
		player.give_hand([card1, card2, card3, card4, card5])

		node = player.get_corresponding_node_idx(card1)
		idx = (14 * (card1.suit - 1)) + card1.number
		assert(node == idx, "TestAIPlayer : Card idx of #{card1.to_s} should be #{idx}... Got #{node} !")

		node = player.get_corresponding_node_idx(card2)
		idx = (14 * (card2.suit - 1)) + card2.number
		assert(node == idx, "TestAIPlayer : Card idx of #{card2.to_s} should be #{idx}... Got #{node} !")

		node = player.get_corresponding_node_idx(card3)
		idx = (14 * (card3.suit - 1)) + card3.number
		assert(node == idx, "TestAIPlayer : Card idx of #{card3.to_s} should be #{idx}... Got #{node} !")

		node = player.get_corresponding_node_idx(card4)
		idx = (14 * (card4.suit - 1)) + card4.number
		assert(node == idx, "TestAIPlayer : Card idx of #{card4.to_s} should be #{idx}... Got #{node} !")

		node = player.get_corresponding_node_idx(card5)
		idx = (14 * (card5.suit - 1)) + (card5.number - 99)
		assert(node == idx, "TestAIPlayer : Card idx of #{card5.to_s} should be #{idx}... Got #{node} !")

		network = player.network
		assert(network.nodes[player.get_corresponding_node_idx(card1)].value == (1.0/15.0), "TestAIPlayer : The node corresponding to #{card1} should have the value #{(1.0/15.0)}... Got #{network.nodes[player.get_corresponding_node_idx(card1)].value}")
		assert(network.nodes[player.get_corresponding_node_idx(card2)].value == (1.0/15.0), "TestAIPlayer : The node corresponding to #{card2} should have the value #{(1.0/15.0)}... Got #{network.nodes[player.get_corresponding_node_idx(card2)].value}")
		assert(network.nodes[player.get_corresponding_node_idx(card3)].value == (1.0/15.0), "TestAIPlayer : The node corresponding to #{card3} should have the value #{(1.0/15.0)}... Got #{network.nodes[player.get_corresponding_node_idx(card3)].value}")
		assert(network.nodes[player.get_corresponding_node_idx(card4)].value == (1.0/15.0), "TestAIPlayer : The node corresponding to #{card4} should have the value #{(1.0/15.0)}... Got #{network.nodes[player.get_corresponding_node_idx(card4)].value}")
		assert(network.nodes[player.get_corresponding_node_idx(card5)].value == (1.0/23.0), "TestAIPlayer : The node corresponding to #{card5} should have the value #{(1.0/23.0)}... Got #{network.nodes[player.get_corresponding_node_idx(card5)].value}")

		played_card = player.play

		assert(played_card.suit == 1 && played_card.number == 1, "TestAIPlayer : The player should have played #{card1.to_s}... Got #{played_card.to_s}")
		assert(network.nodes[player.get_corresponding_node_idx(played_card)].value == 0.0, "TestAIPlayer : The node corresponding to #{played_card} should have the value 0.0... Got #{network.nodes[player.get_corresponding_node_idx(played_card)].value}")

		# assert(network.nodes[78 + player.get_corresponding_node_idx(card1)].value == 1.0, "TestAIPlayer : The node corresponding to #{card1} should have the value 1.0... Got #{network.nodes[78 + player.get_corresponding_node_idx(card1)].value}")
		assert(network.nodes[78 + player.get_corresponding_node_idx(card2)].value == 1.0, "TestAIPlayer : The node corresponding to #{card1} should have the value 1.0... Got #{network.nodes[78 + player.get_corresponding_node_idx(card2)].value}")
		assert(network.nodes[78 + player.get_corresponding_node_idx(card3)].value == 1.0, "TestAIPlayer : The node corresponding to #{card1} should have the value 1.0... Got #{network.nodes[78 + player.get_corresponding_node_idx(card3)].value}")
		assert(network.nodes[78 + player.get_corresponding_node_idx(card4)].value == 1.0, "TestAIPlayer : The node corresponding to #{card1} should have the value 1.0... Got #{network.nodes[78 + player.get_corresponding_node_idx(card4)].value}")
		assert(network.nodes[78 + player.get_corresponding_node_idx(card5)].value == 1.0, "TestAIPlayer : The node corresponding to #{card1} should have the value 1.0... Got #{network.nodes[78 + player.get_corresponding_node_idx(card5)].value}")

		played_card = player.play
		assert(played_card.suit == card2.suit && played_card.number == card2.number, "TestAIPlayer : The player should have played #{card2.to_s}... Got #{played_card.to_s}")
		assert(network.nodes[player.get_corresponding_node_idx(played_card)].value == 0.0, "TestAIPlayer : The node corresponding to #{played_card} should have the value 0.0... Got #{network.nodes[player.get_corresponding_node_idx(played_card)].value}")
	end
end

# Temporary commented because too long
# class SetKittyAI < Test::Unit::TestCase
# 	def test_simple
# 		darwin = Darwin.new
#     	p = AIPlayer.new("rmekarni", darwin.create_life)
#         customHand = Array.new()
# 		customHand.push(Card.new(5, 103))
# 		customHand.push(Card.new(5, 100))
#
# 		customHand.push(Card.new(2, 5))
# 		customHand.push(Card.new(2, 6))
# 		customHand.push(Card.new(2, 7))
# 		customHand.push(Card.new(2, 10))
# 		customHand.push(Card.new(2, 11))
# 		customHand.push(Card.new(2, 12))
# 		customHand.push(Card.new(2, 13))
# 		customHand.push(Card.new(2, 14))
#
# 		customHand.push(Card.new(1, 11))
# 		customHand.push(Card.new(1, 13))
# 		customHand.push(Card.new(1, 14))
#
# 		customHand.push(Card.new(3, 8))
# 		customHand.push(Card.new(3, 9))
# 		customHand.push(Card.new(3, 11))
# 		customHand.push(Card.new(3, 12))
# 		customHand.push(Card.new(3, 13))
# 		customHand.push(Card.new(3, 14))
#
#         customHand.push(Card.new(4, 12))
#
#         p.give_hand(customHand)
#
#         kitty = Array.new()
# 		kitty.push(Card.new(2, 8))
# 		kitty.push(Card.new(2, 9))
# 		kitty.push(Card.new(1, 12))
# 		kitty.push(Card.new(4, 13))
# 		kitty.push(Card.new(4, 14))
# 		kitty.push(Card.new(5, 121))
#
#         init = p.hand_value
#         p.set_kitty(kitty, true)
# 		res = p.hand_value
# 		assert(res >= init,"Test set_kitty : score hand reduce from " + init.to_s + " to " + res.to_s)
# 	end
# end

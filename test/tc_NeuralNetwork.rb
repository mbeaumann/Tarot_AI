require_relative "../lib/NeuralNetwork.rb"

require "test/unit"
require 'pp'

class TestUpdateNode < Test::Unit::TestCase

	def test_simple
		network = NeuralNetwork.new()
		node1 = Node.new(1, 0.0, 1)
		node12 = Node.new(12, 0.0, 1)
		node2 = Node.new(2, 0.0, 2)
		node3 = Node.new(3, 0.0, 0)
		network.add_node(node1)
		network.add_node(node2)
		network.add_node(node3)
		network.add_node(node12)
		network.add_link(Link.new(node1, node3, 1, 1))
		network.add_link(Link.new(node12, node3, 0.5, 1))
		network.add_link(Link.new(node3, node2, 1, 1))
		network.updateNode(1, 1.0)
		network.updateNode(12, 1.0)
		network.nodes.values.each do |node|
			assert(node.value == 1.0, "node value != 1.0")
		end
	end
end

class TestJsonSaves < Test::Unit::TestCase

	def test_simple
		network = NeuralNetwork.new()
		node1 = Node.new(1, 0.0, 1)
		node12 = Node.new(12, 0.0, 1)
		node2 = Node.new(2, 0.0, 2)
		node3 = Node.new(3, 0.0, 0)
		node4 = Node.new(4,0.0,0)

		network.add_node(node4)
		network.add_node(node1)
		network.add_node(node2)
		network.add_node(node3)
		network.add_node(node12)

		network.add_link(Link.new(node1, node3, 1, 1))
		network.add_link(Link.new(node12, node3, 0.5, 1))
		network.add_link(Link.new(node3, node2, 1, 1))

		network.dump("test.json")

		network2 = NeuralNetwork.new()
		network2.load("test.json")

		# network2.nodes = network.nodes.compact

		assert(network.links.length == network2.links.length, "TestJSONSaves : number of links not equals")
		assert(network.nodes.length == network2.nodes.length, "TestJSONSaves : number of nodes not equals")

		# network.nodes = network.nodes.sort_by(&:id)
		# network2.nodes = network2.nodes.sort_by(&:id)

		network.nodes.values do |node|
			neighbours1 = network.getNeighboursOf(node.id)
			neighbours2 = network2.getNeighboursOf(node.id)
			assert(neighbours1.eql?(neighbours2), "TestJSONSaves : Neighbours of node #{node.id} are differents")
		end
		#
		# network.links.each.with_index() do |elem, i|
		# 	# pp network2.links[i]
		# 	assert(elem.equalTest(network2.links[i]), "TestJSONSaves : links number #{i.to_s} are not equals")
		# end
		#
		# network.nodes.each.with_index() do |elem, i|
		# 	# pp network2.nodes[i]
		# 	assert(elem.equalTest(network2.nodes[i]), "TestJSONSaves : nodes number #{i.to_s} are not equals")
		# end
	end
end

class TestSanityChecker < Test::Unit::TestCase

	def test_simple
		network = NeuralNetwork.new()
		nodeInput = Node.new(1, 0.0, 1)
		nodeHidden = Node.new(2, 0.0, 0)
		nodeOutput = Node.new(3, 0.0, 2)
		network.add_node(nodeInput)
		network.add_node(nodeHidden)
		network.add_node(nodeOutput)

		network.add_link(Link.new(nodeHidden, nodeInput, 1, 1))
		network.sanityChecker()
		assert(network.links == {}, "Unhealthy link was not removed")

		network.add_link(Link.new(nodeOutput, nodeHidden, 1, 1))
		network.add_link(Link.new(nodeOutput, nodeInput, 1, 1))
		network.sanityChecker()
		assert(network.links == {}, "Unhealthy link was not removed")
	end
end

class TestCycleIgnore < Test::Unit::TestCase

	def test_simple
		network = NeuralNetwork.new()
		nodeInput = Node.new(1, 0.0, 1)
		nodeHidden1 = Node.new(2, 0.0, 0)
		nodeHidden2 = Node.new(3, 0.0, 0)
		nodeHidden3 = Node.new(4, 0.0, 0)
		nodeOutput = Node.new(5, 0.0, 2)
		network.add_node(nodeInput)
		network.add_node(nodeHidden1)
		network.add_node(nodeHidden2)
		network.add_node(nodeHidden3)
		network.add_node(nodeOutput)

		network.add_link(Link.new(nodeInput, nodeHidden1, 1, 1))
		network.add_link(Link.new(nodeHidden1, nodeHidden2, 1, 1))
		network.add_link(Link.new(nodeHidden2, nodeHidden3, 1, 1))
		network.add_link(Link.new(nodeHidden2, nodeOutput, 1, 1))
		network.add_link(Link.new(nodeHidden3, nodeHidden1, 1, 1))

		begin
			network.updateNode(1, 1.0)
		rescue => exception
			puts exception.to_s
			assert(false, "TestCycleIgnore : function doesn't handle cycle correctly... #{exception.to_s}")
		end
	end
end

class  TestNeighbours < Test::Unit::TestCase

	def test_simple
		network = NeuralNetwork.new()
		nodeInput = Node.new(1, 0.0, 1)
		nodeHidden1 = Node.new(2, 0.0, 0)
		nodeHidden2 = Node.new(3, 0.0, 0)
		nodeHidden3 = Node.new(4, 0.0, 0)
		nodeOutput = Node.new(5, 0.0, 2)
		network.add_node(nodeInput)
		network.add_node(nodeHidden1)
		network.add_node(nodeHidden2)
		network.add_node(nodeHidden3)
		network.add_node(nodeOutput)
		network.add_link(Link.new(nodeInput, nodeHidden1, 1, 1))
		network.add_link(Link.new(nodeHidden1, nodeHidden2, 1, 1))
		network.add_link(Link.new(nodeHidden2, nodeHidden3, 1, 1))
		network.add_link(Link.new(nodeHidden2, nodeOutput, 1, 1))
		network.add_link(Link.new(nodeHidden3, nodeHidden1, 1, 1))
		network.add_link(Link.new(nodeHidden3, nodeOutput, 1, 1))
		n1 = network.getNeighboursOf(1)
		n2 = network.getNeighboursOf(4)
		assert((n1[0].id == 2), "TestNeighbours : 2 should be the neighbour of 1")
		assert((n2[0].id == 2), "TestNeighbours : 2 should be the neighbour of 4")
		assert((n2[1].id == 5), "TestNeighbours : 5 should be the neighbour of 4")
	end

end

class TestCycleDetection < Test::Unit::TestCase

	def test_simple
		network = NeuralNetwork.new()
		nodeInput = Node.new(1, 0.0, 1)
		nodeHidden1 = Node.new(2, 0.0, 0)
		nodeHidden2 = Node.new(3, 0.0, 0)
		nodeHidden3 = Node.new(4, 0.0, 0)
		nodeHidden4 = Node.new(5, 0.0, 0)
		nodeHidden5 = Node.new(6, 0.0, 0)
		nodeOutput = Node.new(7, 0.0, 2)
		network.add_node(nodeInput)
		network.add_node(nodeHidden1)
		network.add_node(nodeHidden2)
		network.add_node(nodeHidden3)
		network.add_node(nodeOutput)

		network.add_link(Link.new(nodeInput, nodeHidden1, 1, 1))
		network.add_link(Link.new(nodeInput, nodeHidden2, 1, 1))
		network.add_link(Link.new(nodeInput, nodeHidden3, 1, 1))
		network.add_link(Link.new(nodeHidden1, nodeHidden4, 1, 1))
		network.add_link(Link.new(nodeHidden2, nodeHidden4, 1, 1))
		network.add_link(Link.new(nodeHidden2, nodeHidden5, 1, 1))
		network.add_link(Link.new(nodeHidden3, nodeHidden5, 1, 1))
		network.add_link(Link.new(nodeHidden4, nodeOutput, 1, 1))
		last = Link.new(nodeHidden5, nodeOutput, 1, 1)
		network.add_link(last)

		assert(!network.isCyclic(last), "TestCycleDetection : Unexistant cycle detected...")

		bad = Link.new(nodeHidden5, nodeInput, 1, 1)
		network.add_link(bad)
		assert(network.isCyclic(bad), "TestCycleDetection : Cycle not detected")
		network.remove_link(bad)

		bad = Link.new(nodeHidden4, nodeHidden2, 1, 1)
		network.add_link(bad)
		assert(network.isCyclic(bad), "TestCycleDetection : Cycle not detected")
		network.remove_link(bad)

		bad = Link.new(nodeOutput, nodeHidden3, 1, 1)
		network.add_link(bad)
		assert(network.isCyclic(bad), "TestCycleDetection : Cycle not detected")
		network.remove_link(bad)

		good = Link.new(nodeHidden4, nodeHidden3, 1, 1)
		network.add_link(good)
		assert(!network.isCyclic(good), "TestCycleDetection : Unexistant cycle detected...")
	end
end

class TestCompatibilityDistance < Test::Unit::TestCase

	def test_simple
		#Creation of the 2 neural networks from page 109 of http://nn.cs.utexas.edu/downloads/papers/stanley.ec02.pdf
        n1 = NeuralNetwork.new()
		n1_nodeInput1 = Node.new(1, 0.0, 1)
        n1_nodeInput2 = Node.new(2, 0.0, 1)
        n1_nodeInput3 = Node.new(3, 0.0, 1)
        n1_nodeOutput = Node.new(4, 0.0, 2)
        n1_nodeHidden = Node.new(5, 0.0, 0)
        n1.add_node(n1_nodeInput1)
        n1.add_node(n1_nodeInput2)
        n1.add_node(n1_nodeInput3)
        n1.add_node(n1_nodeOutput)
        n1.add_node(n1_nodeHidden)
        n1.add_link(Link.new(n1_nodeInput1, n1_nodeOutput, 0.0, 1))
        slink = Link.new(n1_nodeInput2, n1_nodeOutput, 0.0, 2)
        slink.activated = false
        n1.add_link(slink)
        n1.add_link(Link.new(n1_nodeInput3, n1_nodeOutput, 0.0, 3))
        n1.add_link(Link.new(n1_nodeInput2, n1_nodeHidden, 0.0, 4))
        n1.add_link(Link.new(n1_nodeHidden, n1_nodeOutput, 0.0, 5))
        n1.add_link(Link.new(n1_nodeInput1, n1_nodeHidden, 0.0, 8))

        n2 = NeuralNetwork.new()
        n2_nodeInput1 = Node.new(1, 0.0, 1)
        n2_nodeInput2 = Node.new(2, 0.0, 1)
        n2_nodeInput3 = Node.new(3, 0.0, 1)
        n2_nodeOutput = Node.new(4, 0.0, 2)
        n2_nodeHidden1 = Node.new(5, 0.0, 0)
        n2_nodeHidden2 = Node.new(6, 0.0, 0)
        n2.add_node(n2_nodeInput1)
        n2.add_node(n2_nodeInput2)
        n2.add_node(n2_nodeInput3)
        n2.add_node(n2_nodeOutput)
        n2.add_node(n2_nodeHidden1)
        n2.add_node(n2_nodeHidden2)
        n2.add_link(Link.new(n2_nodeInput1, n2_nodeOutput, 0.0, 1))
        slink = Link.new(n2_nodeInput2, n2_nodeOutput, 0.0, 2)
        slink.activated = false
        n2.add_link(slink)
        n2.add_link(Link.new(n2_nodeInput3, n2_nodeOutput, 0.0, 3))
        n2.add_link(Link.new(n2_nodeInput2, n2_nodeHidden1, 0.0, 4))
        slink = Link.new(n2_nodeHidden1, n2_nodeOutput, 0.0, 5)
        slink.activated = false
        n2.add_link(slink)
        n2.add_link(Link.new(n2_nodeHidden1, n2_nodeHidden2, 0.0, 6))
        n2.add_link(Link.new(n2_nodeHidden2, n2_nodeOutput, 0.0, 7))
        n2.add_link(Link.new(n2_nodeInput3, n2_nodeHidden1, 0.0, 9))
        n2.add_link(Link.new(n2_nodeInput1, n2_nodeHidden2, 0.0, 10))

		puts "RESULT = " + n1.compatibility_distance(n2).to_s
	end
end
